<?php
class MmCommonFunctions{
	private static $instance = null;

	static function init() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new MmCommonFunctions();
		}

		return self::$instance;
	}
	
	/**
	 * exchange input to ralatve link
	 * @param  string $input url
	 * @param  string $type  post_type
	 * @return string modify url
	 */
	public function url($input,$type) {
		$url = parse_url($input);
		return '/'.$type.$url['path'];
	}
	/**
	 * get post for multiste *in single page
	 * @return queried object
	 */
	public function get_post($add_params = null,$is_single = true){
		global $wp_query;
		$post_type = $wp_query->query_vars['mm_post_type'];
		$slug = $wp_query->query_vars['mm_name'];

		$post = get_page_by_path($slug , "OBJECT", $post_type);
		$query = new WP_Query();
		$param = array(
			'post_type' => $post_type
		);

		if($post && $is_single){
			$param['p'] = $post->ID;
		}else{
			$param['paged'] = $wp_query->query_vars['paged'];;
		}

		if($add_params){
			$param = array_merge($param,$add_params);
		}

		$query->query($param);
		//echo "Last SQL-Query: {$query->request}";
		return $query;
	}
	/**
	 * get terms for multisite
	 * @param  string $taxonomy taxnomy name
	 * @param  int $blog_id  blog id
	 * @param  boolean $hide_empty
	 * @return object           term infomation object
	 */
	public function get_terms($taxonomy, $blog_id,$hide_empty = true,$exclude = array(),$is_blog = array()){
		switch_to_blog($blog_id);
		global $wpdb;

		if(!$hide_empty){
			$query = "
				SELECT *
				FROM $wpdb->terms
				LEFT JOIN $wpdb->term_taxonomy
					ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
				WHERE $wpdb->term_taxonomy.taxonomy = %s
				ORDER BY $wpdb->terms.term_order ASC
			";
		}else{
			$query = "
				SELECT object_id,name,slug,taxonomy,post_type
				FROM $wpdb->term_relationships
				LEFT JOIN $wpdb->term_taxonomy
					ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id
				LEFT JOIN $wpdb->terms
					ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
				LEFT JOIN $wpdb->posts
					ON $wpdb->posts.ID = $wpdb->term_relationships.object_id
				WHERE $wpdb->term_taxonomy.taxonomy = %s
				GROUP BY $wpdb->term_taxonomy.term_id
				ORDER BY $wpdb->terms.term_order ASC
			";
		}
		$args = array($taxonomy);
		$taxonomies = $wpdb->get_results( $wpdb->prepare( $query, $args ) );
		if($is_blog){
			$exfilters = array();
			$query = "
				SELECT object_id
				FROM $wpdb->term_relationships
				LEFT JOIN $wpdb->term_taxonomy
					ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id
				LEFT JOIN $wpdb->terms
					ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
				LEFT JOIN $wpdb->posts
					ON $wpdb->posts.ID = $wpdb->term_relationships.object_id
				WHERE 
					$wpdb->term_taxonomy.taxonomy = %s
					AND
					$wpdb->terms.name IN (" . $this->escape_array($is_blog) . ")
				GROUP BY $wpdb->term_relationships.object_id
			";
			
			$args = array($taxonomy);
			$filters_base = $wpdb->get_results( $wpdb->prepare( $query, $args ));
			$taxonomies_base = $taxonomies;
			foreach($taxonomies_base as $taxonomy1){
				foreach($filters_base as $filter1){
					if($filter1->object_id == $taxonomy1->object_id){
						$exfilters[] = $taxonomy1->object_id;
					}else{
						continue;
					}
				}
			}
		}

		if($exclude){
			$query = "
				SELECT object_id
				FROM $wpdb->term_relationships
				LEFT JOIN $wpdb->term_taxonomy
					ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id
				LEFT JOIN $wpdb->terms
					ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
				LEFT JOIN $wpdb->posts
					ON $wpdb->posts.ID = $wpdb->term_relationships.object_id
				WHERE 
					$wpdb->term_taxonomy.taxonomy = %s
					AND
					$wpdb->terms.name IN (" . $this->escape_array($exclude) . ")
				GROUP BY $wpdb->term_relationships.object_id
			";
			
			$args = array($taxonomy);
			$filters = $wpdb->get_results( $wpdb->prepare( $query, $args ));
			$check = array();
			foreach($taxonomies as $taxonomy){
				if($exfilters && !in_array($taxonomy->object_id,$exfilters)){
					continue;
				}

				if($filters){
					foreach($filters as $filter){
						if($filter->object_id == $taxonomy->object_id){
							continue;
						}else{
							if(!in_array($taxonomy->slug,$check)){
								$results[] = $taxonomy;
								$check[] = $taxonomy->slug;
							}
						}
					}
				}else{
					if(!in_array($taxonomy->slug,$check)){
						$results[] = $taxonomy;
						$check[] = $taxonomy->slug;
					}
				}
			}
		}else{
			$results = $taxonomies;
		}
		restore_current_blog();
		return $results;
	}

	/**
	 * get the terms for multisite
	 * @param  string $taxonomy taxnomy name
	 * @param  int $blog_id  blog id
	 * @param  int $post_id  post id
	 * @return object           term infomation object
	 */
	public function get_the_terms($taxonomy, $blog_id, $post_id) {
		switch_to_blog($blog_id);
		global $wpdb;
		$query = "
		SELECT *
		FROM $wpdb->term_relationships
		LEFT JOIN $wpdb->term_taxonomy
			ON $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id
		LEFT JOIN $wpdb->terms
			ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id
		WHERE $wpdb->term_relationships.object_id = %d
			AND $wpdb->term_taxonomy.taxonomy = %s
		ORDER BY $wpdb->terms.term_order ASC
		";
		$args = array($post_id, $taxonomy);
		$taxonomies = $wpdb->get_results( $wpdb->prepare( $query, $args ) );
		restore_current_blog();
		return $taxonomies;
	}

	public function get_term_link($slug){
		$post_type = $wp_query->query_vars['mm_post_type'];
	}

	/**
	 * escape array values for wpdb
	 * @param  array 
	 * @return string 
	 */
	public function escape_array($arr){
		global $wpdb;
		$escaped = array();
		foreach($arr as $k => $v){
			if(is_numeric($v))
				$escaped[] = $wpdb->prepare('%d', $v);
			else
				$escaped[] = $wpdb->prepare('%s', $v);
		}
		return implode(',', $escaped);
	}

	/**
	 * reeturn original ID for post type 'case'
	 * @return array post_id => nm
	 */
	public function return_case_num(){
		global $post;
		switch_to_blog(1);
		$myQuery = new WP_Query();
		$param = array(
			'post_type' => 'case',
			'posts_per_page' => -1,
			'order' => 'ASC'
		);
		$myQuery->query($param);
		$id_and_num = array();
		$loopnum = 1;
		if($myQuery->have_posts()){
			while($myQuery->have_posts()){
				$myQuery->the_post();
				$id_and_num[$post->ID] = $loopnum;
				$loopnum++;
			}
		}
		wp_reset_postdata();
		restore_current_blog();


		return $id_and_num;
	}

	/**
	 * get first img in post content
	 * @return string img path
	 */
	public function getPostFirstImg() {
		global $post, $posts;
		$first_img = '';
		$permalink = get_permalink();
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		$first_img = $matches [1] [0];

		if(empty( $matches [1] [0])){ //画像が無いときは表示しない
			$first_img ='';
		}
		return $first_img;
	}

}
