<?php
/*
Plugin Name: mm_multisite_supporter
Description: マルチサイト用のサポート機能を提供します
Author: Yuzuru Sano
Version: 0.1
*/

include_once( dirname( __FILE__ ) . '/inc/functions.php' );
add_action( 'init', array( 'MmCommonFunctions', 'init' ) );
$mmcf = new MmCommonFunctions;


class MMMultiSiteSupporter {
	private static $instance = null;
	/**
	 * set singleton object
	 * @return object
	 */
	static function init() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new MMMultiSiteSupporter();
		}

		return self::$instance;
	}

	protected function __construct() {
		if(get_current_blog_id() == 1){
			add_action('wp', array($this,'redirect_top_contents'),1);
			add_action( 'wp_enqueue_scripts', array($this,'add_map_js') );
		}else{
			add_action( 'init', array($this,'posttype_archive_link_flush') );
			add_action( 'wp_enqueue_scripts', array($this,'add_map_js') );
			add_action( 'wp_head', array($this,'add_meta_info'),1);
			add_action( 'template_redirect', array($this,'front_controller') );
			add_filter( 'query_vars', array($this,'add_mm_data_to_vars') );
			add_filter( 'get_pagenum_link', array($this,'fix_rr'), 10, 1 );
			add_filter('content_save_pre',array($this,'allow_iframe_tag'),1, 1);
			add_filter( 'wpseo_metadesc', array($this,'remove_metadesc'),9999,1 );
			add_filter( 'wpseo_canonical', array($this,'remove_wpseo_canonical'), 9999, 1 );
		}
	}
	/**
	 * register route
	 * @return [type] [description]
	 */
	public function posttype_archive_link_flush(){
		$post_types = array(
			'case',
			'blog',
			'movie'
		);

		foreach($post_types as $pt){
			add_rewrite_rule(
				$pt.'/([^/]+)?$',
				'index.php?mm_post_type='.$pt.'&mm_name=$matches[1]',
				'top'
			);
			add_rewrite_rule(
				$pt.'/category/([^/]+)?$',
				'index.php?mm_post_type='.$pt.'&mm_term=$matches[1]&mm_taxonomy=category_'.$pt,
				'top'
			);
			add_rewrite_rule(
				$pt.'/category/([^/]+)/page/?([0-9]{1,})/?$',
				'index.php?mm_post_type='.$pt.'&mm_term=$matches[1]&mm_taxonomy=category_'.$pt.'&paged=$matches[2]',
				'top'
			);
			add_rewrite_rule(
				$pt.'/?$',
				'index.php?mm_post_type='.$pt,
				'top'
			);
			add_rewrite_rule(
				$pt.'/page/([0-9]{1,})/?$',
				'index.php?mm_post_type='.$pt.'&paged=$matches[1]',
				'top'
			);
		}
	}

	/**
	 * register query vars
	 * @param array
	 */
	public function add_mm_data_to_vars( $query_vars ) {
		$query_vars[] = 'mm_post_type';
		$query_vars[] = 'mm_name';
		$query_vars[] = 'mm_term';
		$query_vars[] = 'mm_taxonomy';
		return $query_vars;
	}

	/**
	 * set template redirect
	 * @return none
	 */
	public function front_controller() {
		global $wp_query;
		$post_type = isset ( $wp_query->query_vars['mm_post_type'] ) ? $wp_query->query_vars['mm_post_type'] : '';
		$name = isset ( $wp_query->query_vars['mm_name'] ) ? $wp_query->query_vars['mm_name'] : '';

		if($post_type && $name){
			switch_to_blog(1);
				$args = array(
					'name'           => $name,
					'post_type'      => $post_type,
					'post_status'    => 'publish',
					'posts_per_page' => 1
				);
				$exists_posts = get_posts( $args );
			restore_current_blog();

			if(empty($exists_posts)){
				header("HTTP/1.1 404 Not Found");
				include get_template_directory() . '/404.php';
				exit;
			}

			do_action('render_'.$post_type.'_single');
			include get_template_directory() . '/single-'.$post_type.'.php';
			exit;
		}elseif($post_type){
			do_action('render_'.$post_type.'_archive');
			include get_template_directory() . '/archive-'.$post_type.'.php';
			exit;
		}
	}

	/**
	 * return fxed path
	 * @param  string $result
	 * @return string
	 */
	public function fix_rr($result){
		$url = parse_url($result);
		return $url['path'];
	}

	/**
	 * support meta info
	 */
	public function add_meta_info() {
		global $mmcf;
		global $post; 
		global $wp_query;
		global $more;
		$wpseo_frontend = WPSEO_Frontend::get_instance();
		$current_blog_id = get_current_blog_id();
		$this_blog_info = get_bloginfo('name');
		$url =  (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

		$mm_post_type = $wp_query->query_vars['mm_post_type'];
		$mm_name = $wp_query->query_vars['mm_name'];
		$mm_term = $wp_query->query_vars['mm_term'];
		$mm_taxonomy = $wp_query->query_vars['mm_taxonomy'];

		if(!$mm_post_type){
			return;
		}

		switch_to_blog(1);
			$config = require_once('config/config.php');
			if(!$mm_term && !$mm_taxonomy && $mm_name){
				$query = $mmcf->get_post();
				if($query->have_posts()){
					while($query->have_posts()){
						$query->the_post(); 
						$more = 1;
						if ( has_post_thumbnail() ){
							$imgid = get_post_thumbnail_id();
							$img = wp_get_attachment_image_src( $imgid, 'photo280-9999', $icon );
							$src = $img[0];
						}else{
							$src = get_stylesheet_directory_uri().'/assets/images/img_blog_default.png';
						}
						$title = get_the_title().' | '.$this_blog_info ;
						$output = '<title>'.$title .'</title>';
						
						$content = get_the_content();
						$content = preg_replace('/[\r\n|\n\r|\n|\r]/', '', $content);
						$content = mb_substr(strip_tags($content),0,100);

						$first_img = '';
						preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

						if(!empty( $matches [1] [0])){ //画像が無いときは表示しない
							$first_img = $matches [1] [0];
							$str = $first_img;
							$Extensionstr = substr($str, -4);
							$Imagestr = substr($str, 0, -4);
							$ogimg = $Imagestr.$Extensionstr;
						}
					}
				}else{
					$title = $config[$current_blog_id][$mm_post_type]['archive']['title'].' | '.$this_blog_info;
					$output = '<title>'.$title .'</title>';
				}
			}elseif(!$mm_term && !$mm_taxonomy && !$mm_name && $mm_post_type){
				$title = $config[$current_blog_id][$mm_post_type]['archive']['title'].' | '.$this_blog_info;
				$output = '<title>'. $title.'</title>';
			}elseif($mm_term && $mm_taxonomy){
				$term = get_term_by('slug',$mm_term,$mm_taxonomy);
				$title = $term->name.' | '.$config[$current_blog_id][$mm_post_type]['archive']['title'].' | '.$this_blog_info;
				$output = '<title>'.$title .'</title>';
				$content = $term->name.'に関連する'.$config[$current_blog_id][$mm_post_type]['archive']['desc'];
			}
			if(!$content){
				$content = $config[$current_blog_id][$mm_post_type]['archive']['desc'];
			}
		restore_current_blog();

		if(!$ogimg && is_object($wpseo_frontend)){
			$ogimg = $wpseo_frontend->options['og_frontpage_image'];
		}

		$meta = '
		<meta name="description" content="%s"/>
		<meta property="og:locale" content="ja_JP" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="%s" />
		<meta property="og:description" content="%s" />
		<meta property="og:url" content="%s" />
		<meta property="og:site_name" content="%s" />
		<meta property="og:image" content="%s" />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:description" content="%s" />
		<meta name="twitter:title" content="%s" />
		<meta name="twitter:image" content="%s" />
		<link rel="canonical" href="%s" />
		';
		$output .= sprintf($meta,$content,$title,$content,$url ,$this_blog_info,$ogimg ,$content,$title,$ogimg,$url);
		echo $output;
	}

	/**
	 * remove meta desc for custom route
	 * @param  string $metadesc
	 * @return string
	 */
	public function remove_metadesc($metadesc){
		global $wp_query; 

		if($wp_query->query_vars['mm_post_type'] || $wp_query->query_vars['mm_name'] || $wp_query->query_vars['mm_term'] || $wp_query->query_vars['mm_taxonomy']){
			return '';
		}else{
			return $metadesc;
		}
	}
	/**
	 * remove canonical tag  for custom route
	 * @param  strng $canonical
	 * @return string
	 */
	public function remove_wpseo_canonical( $canonical ) {
		global $wp_query;
		if($wp_query->query_vars['mm_post_type']){
			return '';
		}else{
			return $canonical;
		}
	}

	/**
	 * stop removing iframe on vsual editor
	 * @param  string $content
	 * @return string
	 */
	public function allow_iframe_tag($content){
		global $allowedposttags;
		$allowedposttags['iframe'] = array('class' => array () , 'src'=>array() , 'width'=>array(),
		'height'=>array() , 'frameborder' => array() , 'scrolling'=>array() , 'marginheight'=>array(),
		'marginwidth'=>array() , 'allowfullscreen'=>array());
		return $content;
	}

	/**
	 * redirect top site contents to related site
	 * @return null
	 */
	public function redirect_top_contents(){
		if(!is_user_logged_in() && get_current_blog_id() == 1 && !is_admin()){
			global $wp_query;
			global $post;

			$high = 'http://high.manabinomori.co.jp';
			$free = 'http://free.manabinomori.co.jp';
			$post_type = $wp_query->query_vars['post_type'];
			$grade = array('小学生','中学生','高校生');
			$g1 = false;
			$g2 = false;
			$g3 = false;

			$recirect_url = '';

			if(is_single() && in_array($post_type,array('blog','case'))){
				$url = '/'.$post_type.'/'.$post->post_name;
				$terms = get_the_terms( $post->ID, 'category_'. $post_type);
				foreach($terms as $term){
					$name = $term->name;
					switch ($name) {
						case '小学生':
							$g1 = true;
							break;
						case '中学生':
							$g2 = true;
							break;
						case '高校生':
							$g3 = true;
							break;
						default:
							break;
					}
				}

				if($g1 && $g2 && $g3){
					$recirect_url = $free.$url;
				}elseif($g3){
					$recirect_url = $high.$url;
				}elseif(($g1 || $g2) && !$g3){
					$recirect_url = $free.$url;
				}else{
					$recirect_url = $free;
				}
			}elseif(is_archive() && in_array($post_type,array('blog','case'))){
				$recirect_url = $free.'/'.$post_type;
			}elseif(is_tax()){
				$blog_cat = $wp_query->query_vars['category_blog'];
				$case_cat = $wp_query->query_vars['category_case'];

				if($blog_cat){
					$post_type = 'blog';
					$cat = $blog_cat;
				}elseif($case_cat){
					$post_type = 'case';
					$cat = $case_cat;
				}

				if($cat == 'cat3'){
					$recirect_url = $high.'/'.$post_type.'/category/'.$cat;
				}else{
					$recirect_url = $free.'/'.$post_type.'/category/'.$cat;
				}
			}elseif(is_page()){
				$recirect_url = $free.'/'.$post->post_name;
			}elseif(is_archive() && in_array($post_type,array('movie','teacher'))){
				$recirect_url = $free.'/'.$post_type;
			}

			if($recirect_url){
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".$recirect_url);
				exit();
			}
		}
	}
	/**
	 * add googlemap after page loaded
	 */
	public function add_map_js(){
		if(is_page('access')){
			wp_enqueue_script('map-js', plugins_url().'/mm_multisite_supporter/js/map.js', array('jquery'), '', true);
		}
	}
}

MMMultiSiteSupporter::init();