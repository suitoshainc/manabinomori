<?php
/*
Plugin Name: mm_admin_supporter
Description: 管理画面サポート機能を提供します
Author: Yuzuru Sano
Version: 0.1
*/

class MMAdminSupporter {
	private static $instance = null;

	static function init() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new MMAdminSupporter();
		}

		return self::$instance;
	}

	protected function __construct() {
		add_action( 'admin_init', array($this,'adminMenuControl'),1 );
		add_action('admin_bar_menu', array($this,'adminBarMenuControl'), 201);
		add_action('admin_menu', array($this,'adminMenuControl'),9999);
		add_action('wp_dashboard_setup',array($this,'removeDashBoardsWidgets'),9999);
	}

	public function adminMenuControl(){
		global $current_user;
		global $menu;
		wp_get_current_user();
		if($current_user->user_level == 7){
			//unset($menu[2]);  // ダッシュボード
			unset($menu[4]);  // メニューの線1
			unset($menu[5]);  // 投稿
			//unset($menu[10]); // メディア
			unset($menu[15]); // リンク
			//unset($menu[20]); // ページ
			unset($menu[25]); // コメント
			unset($menu[59]); // メニューの線2
			unset($menu[60]); // テーマ
			unset($menu[65]); // プラグイン
			unset($menu[70]); // プロフィール
			unset($menu[75]); // ツール
			unset($menu[80]); // 設定
			unset($menu[90]); // メニューの線3

			remove_menu_page('wpcf7');
			remove_menu_page('ps-taxonomy-expander.php');
		}
	}

	public function removeDashBoardsWidgets() {
		global $current_user;
		wp_get_current_user();
		if($current_user->user_level == 7){ //level10以下のユーザーの場合ウィジェットをunsetする
			global $wp_meta_boxes;
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // 現在の状況
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
		}
	}


	public function adminBarMenuControl($wp_admin_bar){
		$wp_admin_bar->remove_menu( 'wpseo-menu' );

		global $current_user;
		wp_get_current_user();
		if($current_user->user_level == 7){
			//$wp_admin_bar->remove_menu( 'my-sites' );
			$wp_admin_bar->remove_menu( 'wp-logo' );      // ロゴ
			//$wp_admin_bar->remove_menu( 'site-name' );    // サイト名
			//$wp_admin_bar->remove_menu( 'view-site' );    // サイト名 -> サイトを表示
			$wp_admin_bar->remove_menu( 'dashboard' );    // サイト名 -> ダッシュボード (公開側)
			$wp_admin_bar->remove_menu( 'themes' );       // サイト名 -> テーマ (公開側)
			$wp_admin_bar->remove_menu( 'customize' );    // サイト名 -> カスタマイズ (公開側)
			$wp_admin_bar->remove_menu( 'comments' );     // コメント
			$wp_admin_bar->remove_menu( 'updates' );      // 更新
			$wp_admin_bar->remove_menu( 'view' );         // 投稿を表示
			$wp_admin_bar->remove_menu( 'new-content' );  // 新規
			$wp_admin_bar->remove_menu( 'new-post' );     // 新規 -> 投稿
			$wp_admin_bar->remove_menu( 'new-media' );    // 新規 -> メディア
			$wp_admin_bar->remove_menu( 'new-link' );     // 新規 -> リンク
			$wp_admin_bar->remove_menu( 'new-page' );     // 新規 -> 固定ページ
			$wp_admin_bar->remove_menu( 'new-user' );     // 新規 -> ユーザー
			// $wp_admin_bar->remove_menu( 'my-account' );   // マイアカウント
			// $wp_admin_bar->remove_menu( 'user-info' );    // マイアカウント -> プロフィール
			// $wp_admin_bar->remove_menu( 'edit-profile' ); // マイアカウント -> プロフィール編集
			// $wp_admin_bar->remove_menu( 'logout' );       // マイアカウント -> ログアウト
			$wp_admin_bar->remove_menu( 'search' );       // 検索
		}
	}
}

MMAdminSupporter::init();