<?php 
get_header(); 
global $mmcf;
global $post; 
global $wp_query;
$blog_top = home_url('blog');
switch_to_blog(1);
$mm_term = $wp_query->query_vars['mm_term'];
$mm_taxonomy = $wp_query->query_vars['mm_taxonomy'];
?>
<header class="lower_header lower_header--blog">
	<h1 class="ttl"><span>日誌</span></h1>
</header>
<div class="backgrond-color--lightblue nosep">
	<?php include( locate_template('modules/blog_archive_lead.php', false, false ) );?>
	<section class="blog_list blog_list--lower">
		<?php 
		$param = array(
			'posts_per_page' => 12,
		);
		//base tax
		$param['tax_query'] = array(
			array(
				'taxonomy'=>'category_blog',
				'terms'=>array( 'cat3'),
				'field'=>'slug'
			)
		);
		//paged
		if($paged = $wp_query->query_vars['paged']){
			$param['paged'] = $paged;
		}
		//tax query
		if($mm_term && $mm_taxonomy){
			$param['tax_query'][] = array(
				'taxonomy' => 'category_blog',
				'terms'=> $mm_term,
				'field'=> 'slug'
			);
			
		}
		
		if(isset($_GET['un']) && $_GET['un']){
			$param['author_name'] = htmlspecialchars($_GET['un']);
		}
		$query = $mmcf->get_post($param,false);
		include( locate_template('modules/blog_archive_loop.php', false, false ) );
		if(function_exists('wp_pagenavi')) {wp_pagenavi(array('query' => $query ));}
			wp_reset_postdata();
		?>
	</section>
</div>
<?php restore_current_blog(); ?>
<?php get_footer(); ?>
