<?php get_header(); ?>

<section class="wrap--list-qa">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<dl class="list-qa">
			<dt class="ttlarea">
				<h3 class="inner">
					<div class="num">Q 1</div>
					<div class="ttl"><?php the_title();?></div>
				</h3>
			</dt>
			<dd class="txt"><?php the_content();?></dd>
		</dl>
	<?php endwhile; ?>
	<?php else : ?>
		<h2 class="title">記事が見つかりませんでした。</h2>
		<p>検索で見つかるかもしれません。</p><br />
		<?php get_search_form(); ?>
	<?php endif; ?>
</section>

<?php get_footer(); ?>
