<?php 
get_header(); 
global $mmcf;
global $post; 
global $wp_query;
switch_to_blog(1);
$mm_term = $wp_query->query_vars['mm_term'];
$mm_taxonomy = $wp_query->query_vars['mm_taxonomy'];
?>
<header class="lower_header lower_header--case">
	<h1 class="ttl"><span>日誌</span></h1>
</header>
<div class="backgrond-color--lightblue nosep">
	<?php include( locate_template('modules/blog_archive_lead.php', false, false ) );?>

	<section class="blog_list blog_list--lower">
		<?php 
		$param = array(
			'posts_per_page' => 12
		);
		$param['tax_query'] = array(
			'relation' => 'AND',
			array(
				'taxonomy'=>'category_blog',
				'terms'=>array( 'cat2','cat1'),
				'field'=>'slug',
				'operator'=>'NOT IN'
			)
		);
		if($mm_term && $mm_taxonomy){
			$param['tax_query'][] = array(
					'taxonomy' => $mm_taxonomy,
					'terms'=> $mm_term,
					'field'=> 'slug'
				);
			
		}
		$query = $mmcf->get_post($param );
		?>
		<?php include( locate_template('modules/blog_archive_loop.php', false, false ) );?>
		<?php 
		if(function_exists('wp_pagenavi')) {wp_pagenavi(array('query' => $query ));}
			wp_reset_postdata();
		?>
	</section>
</div>
<?php restore_current_blog(); ?>
<?php get_footer(); ?>

