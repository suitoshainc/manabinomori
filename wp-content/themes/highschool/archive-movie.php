<?php 
get_header(); 
global $mmcf;
global $post; 
global $wp_query;
switch_to_blog(1);
?>
<header class="lower_header lower_header--movie">
	<h1 class="ttl"><span>movie</span></h1>
</header>
<div class="backgrond-color--lightblue nosep movie_list--wrap">
	<?php 
	$terms = $mmcf->get_terms('category_movie',1);
	$tags = array();
	if($terms):
	foreach($terms as $term):
	?>
		<section class="blog_list blog_list--lower movie_list">
			<h2 class="main_ttl"><?php echo $term->name;?></h2>
			<?php 
			$param = array(
				'posts_per_page' => -1
			);
			$param['tax_query'] = array(
				array(
					'taxonomy' => 'category_movie',
					'terms'=> $term->slug,
					'field'=> 'slug'
				)
			);
			$query = $mmcf->get_post($param,false);
			?>
			<ul class="list">
				<?php if($query->have_posts()): while($query->have_posts()) : $query->the_post(); ?>
					<li>
						<div class="movie">
							<iframe width="300" height="168" src="https://www.youtube.com/embed/<?php echo get_field('youtube_link');?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="txt">
							<h3 class="ttl"><?php the_title();?></h3>
						</div>
					</li>
				<?php endwhile;endif; ?>
			</ul>
		</section>
		<?php wp_reset_postdata();?>
	<?php endforeach;endif;?>
	<?php restore_current_blog(); ?>
	<p class="ac movie_btn"><a target="_blank" href="https://www.youtube.com/channel/UCbD4yk-8WJMpNzXxo6C9sUQ" class="no_icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_movie_more.png" alt="もっと見る" width="320"></a></p>
</div>
<?php get_footer(); ?>
