import 'jquery-colorbox';
import Responsive from './Responsive';
const r = new Responsive();

module.exports = ()=>{
	/* ===============================================
	ajax colorbox
	=============================================== */
	$('.execColorBox').on('click',function(){
		let target = $(this).attr('href');
		let param = {
			inline:true,
			width:'100%',
			innerWidth:'100%',
			maxWidth:"560px",
			fixed:'true',
			href:target,
			opacity:0.8,
			onOpen: function(){
				$("body").addClass('c_open');
				$("#colorbox").css({'opacity':0}).addClass('open');
			},
			onComplete:function(){
				$('#colorbox').animate({'opacity':1},800);
			},
			onClosed:function(){
				$("body").removeClass('c_open');
				$('#colorbox').css({'opacity':0}).removeClass('open');
			}
		};

		if(r.state()()){
			param.width = '88%';
			param.innerWidth = '88%';
			param.maxWidth = '88%';
			param.fixed = false;
		}

		$.colorbox(param);
	});
};
