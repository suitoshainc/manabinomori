window.$ = jQuery;
import Responsive from './Responsive';
import SmoothScroll from './SmoothScroll';
import Accordion from './Accordion';
import Tab from './Tab';
import SpNavi from './SpNavi';
import ToTop from './ToTop';
import DropDownMenu from './DropDownMenu';
import Wave from './Wave';
import BlockLink from './BlockLink';
import Inview from './Inview';
import AjaxColorBox from './AjaxColorbox.js';
const bxslider = require('../../lib/js/bxslider-4/jquery.bxSlider.min.js');
const lineup = require('../../lib/js/jquery-lineup/jquery-lineup.min.js');
const bowser = require('../../lib/js/bowser/bowser.js');

if(bowser.msie === true) {
	$('body').addClass('ie');
}else if(bowser.msedge === true){
	$('body').addClass('edge');
}
/* ===============================================
Check responsive state
=============================================== */
const r = new Responsive();
//console.log(r.state()());

if(!r.isPhone()){
	$('.tel_link , .tel_link a').on('click',function(){
		return false;
	});
}
/* ===============================================
no link
=============================================== */
$('.nolink').on('click',function(){
	return false;
});
/* ===============================================
slider
=============================================== */
param = {
	mode:'fade',
	speed: 1000,
	controls: true,
	pause: 4000,
	pager:false,
	auto: true
};
$('.slide ul').bxSlider(param);
/* ===============================================
wave
=============================================== */
let wave1 = new Wave('wave1',35,GROBAL_WAVE_BACKGROUND,'above',2,false,90,false,9);
let wave2 = new Wave('wave2',35,'#f8f7f1','bottom',2,false,200,false,9);
wave1.init();
wave2.init();
$(window).on('resize',function(){
	wave1.setCanvas();
	wave1.draw();
	wave2.setCanvas();
	wave2.draw();
});
/* ===============================================
roolover
=============================================== */
$('img.rollover').mouseover(function(){
	$(this).attr('src',$(this).attr('src').replace(/^(.+)_off(\.[a-z]+)$/, '$1_on$2'));
}).mouseout(function(){
	$(this).attr('src',$(this).attr('src').replace(/^(.+)_on(\.[a-z]+)$/, '$1_off$2'));
}).each(function(){
	$("<img>").attr("src",$(this).attr("src").replace(/^(.+)_off(\.[a-z]+)$/, "$1_on$2"));
});
/* ===============================================
box link
=============================================== */
let borer_area_link = new BlockLink();
borer_area_link.exec('.border-area');
/* ===============================================
SP Navi
=============================================== */
let param = {
	target:'#spnavi',
	trigger:'.btn_sp_navi',
	filter:'resposive_flg',
	speed:200,
	fixer:80
};
const spnavi = new SpNavi(param);
spnavi.exec();
/* ===============================================
Smooth Scroll
=============================================== */
const sms = new SmoothScroll();
sms.exec();
/* ===============================================
To Top
show/hide toggle
=============================================== */
// const totop = new ToTop('#totop',100,400);
// totop.exec();
/* ===============================================
DropDown

markp:
<ul>
	<li>
		<a href=""></a>
		<ul class="child">
			<li>
				<a href=""></a>
			</li>
			<li>
				<a href=""></a>
			</li>
			<li>
				<a href=""></a>
			</li>
		</ul>
	</li>
	<li>
		<a href=""></a>
	</li>
	<li>
		<a href=""></a>
	</li>
</ul>
=============================================== */
// const dd = new DropDownMenu('.gnav ul li ,.footer_nav ul li','.child',200);
// dd.exec();
_.each($('.gnav ul li'),(item)=>{
	let child = $(item).find('.child');
	if(child.length > 0){
		$(item).on('mouseenter',function(){
			child.fadeIn(300);
		})
		.on('mouseleave',function(){
			child.fadeOut(300);
		});

	}
});
/* ===============================================
/* ===============================================
Accordion

markp:
<dl>
	<dt></dt>
	<dd></dd>
	<dt></dt>
	<dd></dd>
	<dt></dt>
	<dd></dd>
</dl>
=============================================== */
const ac = new Accordion($('dt.ttlarea'));
ac.exec();
$('.footer_nav > ul > li:odd').addClass('even');
$('.footer_nav > ul > li:even').addClass('odd');

/* ===============================================
inview
=============================================== */
const iv = new Inview();
iv.load('.inview--to_bottom' , 'toBottom', 0.8);

iv.load('.inview--to_top--delay1' , 'toTop', 0.8 , -30,0.1);
iv.load('.inview--to_top--delay2' , 'toTop', 0.8 , -30,0.3);
iv.load('.inview--to_top--delay3' , 'toTop', 0.8 , -30,0.5);
iv.load('.inview--to_top' , 'toTop',0.5,20,0,false);

/* ===============================================
page_nav
=============================================== */
let page_nav = $('.page_nav').find('a');
$(window).on('load resize',function(){
	if(r.state()() === true){
		page_nav.addClass('nosms');
		page_nav.removeClass('notab');
	}else{
		page_nav.removeClass('nosms');
		page_nav.addClass('notab');
	}
});
/* ===============================================
Tab

markup:
<div class="tc">
	<ul>
		<li>tab1</li>
		<li>tab2</li>
		<li>tab3</li>
	</ul>
	<div class="tc_content">
		tabcon1tabcon1tabcon1tabcon1tabcon1tabcon1
	</div>
	<div class="tc_content">
		tabcon2tabcon2tabcon2tabcon2tabcon2tabcon2
	</div>
	<div class="tc_content">
		tabcon3tabcon3tabcon3tabcon3tabcon3tabcon3
	</div>
</div>
=============================================== */
const tab = new Tab('.page_nav a','.tab_content');
tab.exec();

/* ===============================================
form
=============================================== */
_.each($('.basic_form').find('select'),function(item){
	let $select = $(item);

	let $dummy = $select.next('.dummy_select');

	let default_val = $select.find("option:selected").text();
	$dummy.val(default_val);

	$select.change(function(){
		let val = $(this).find("option:selected").text();
		$dummy.val(val);
	});
});
/* ===============================================
blank
=============================================== */
$('a[href*="http"]').not('[href*="'+location.hostname+'"]').not('.no_icon').not('.not_blank').attr({target:"_blank"}).addClass('blank');
/* ===============================================
lineup
=============================================== */
$(window).on('load',function(){
	$('.border-area .content_area_wrapper .row').each(function(){
		$(this).find('.block__base_height').lineUp();
	});
	$(".card--teacher").lineUp();
	setTimeout(
		function(){
			$(".card--teacher .content").lineUp();
		},200
	);
	$('.blog_list .list li').lineUp();
});
/* ===============================================
blog list
=============================================== */
$('.blog_list li').on('mouseenter',function(){
	$(this).find('.filter').fadeIn(300);
})
.on('mouseleave',function(){
	$(this).find('.filter').fadeOut(300);
});
/* ===============================================
colorbox
=============================================== */
AjaxColorBox();
