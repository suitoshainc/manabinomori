<?php 
global $mmcf; 
global $wp_query;
global $more;
get_header(); 
$this_theme_dir = get_stylesheet_directory_uri();
?>
<header class="lower_header lower_header--blog">
	<h1 class="ttl"><span>日誌</span></h1>
</header>
<div class="backgrond-color--lightblue nosep">
	<?php 
	$single = true;
	$blog_top = home_url('blog');
	include( locate_template('modules/blog_archive_lead.php', false, false ) );
	?>
	<?php switch_to_blog(1);?>
	<article class="content_area content_area--blog">
		<?php 
		$p = $mmcf->get_post();
		if($p->have_posts()): while($p->have_posts()) : $p->the_post(); 
		$base_id = $post->ID;
		$more = 1;
		$author = get_userdata($post->post_author)->display_name;
		if($author == 'mm_siteadmin'){
			$author = '学びの森';
		}
		?>
		<div class="main_ttl">
			<h2 class="ttl"><span><?php the_title();?></span></h2>
		</div>
		<div class="inner">
			<?php the_content();?>
			<aside class="post_meta">
				<?php include( locate_template('/modules/blog_tags.php', false, false ) );?>
				<p class="date"><?php the_time('Y / m / d')?>　<a href="/blog/?un=<?php echo get_userdata($post->post_author)->user_nicename;?>"><?php echo $author;?></a></p>
			</aside>
		</div>
		<?php endwhile;endif; wp_reset_postdata();?>
		<aside class="prev_next">
			<ul>
			<?php 
				$postlist_args = array(
					'posts_per_page' => -1,
					'post_type' => $wp_query->query_vars['mm_post_type'],
					'tax_query' => array(
						array(
							'taxonomy' => $term->taxonomy,
							'field' => 'slug',
							'terms' => 'cat3',//高校生
							'include_children' => true,
						)
					)
				);
				$postlist = get_posts( $postlist_args ); 
				$ids = array(); 
				$lc=0;
				foreach ($postlist as $thepost) {
					$lc = $lc+10;
					$ids[] = $thepost->ID; 
				} 
				
				$thisindex = array_search($base_id, $ids); 
				$previd = $ids[$thisindex-1]; 
				$nextid = $ids[$thisindex+1]; 
				if ( !empty($previd) ) { 
					echo '<li class="prev"><a rel="prev" href="' . $mmcf->url(get_permalink($previd),'blog'). '">'.get_the_title($previd).'</a></li>'; 
				} 
				if ( !empty($nextid) ) { 
					echo '<li class="next"><a rel="next" href="' . $mmcf->url(get_permalink($nextid),'blog'). '">'.get_the_title($nextid).'</a></li>'; 
				}
			?>
		</ul>
		</aside>
	</article>
	<?php 
	$half_ymdd = time() - 180*24*60*60;
	$param = array(
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post__not_in' => array($base_id),
		'tax_query' => array(
			array(
				'taxonomy' => 'category_blog',
				'field' => 'slug',
				'terms' => 'cat3',//高校生
			)
		),
		'date_query' => array(
			array(
				'compare'=>'BETWEEN',
				'inclusive'=>true,
				'after'=>date('Y/m/d',$half_ymdd),
				'before'=>date('Y/m/d') 
			),
		),
	);

	$query = $mmcf->get_post($param,false);
	$layout_simple = true;
	include( locate_template('modules/blog_archive_loop.php', false, false ) );
	wp_reset_postdata();

	restore_current_blog();
	?>
</div>
<script>
	jQuery(document).ready(function() {
		$('.content_area--blog a:has(img)').addClass('fancybox');
		easy_fancybox_handler();
	});
</script>
<?php get_footer(); ?>
