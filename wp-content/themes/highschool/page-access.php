<?php get_header();switch_to_blog(1); global $mmcf;?>
<?php
	$myQuery = new WP_Query();
	$param = array(
		'pagename' => 'access',
		'post_status' => array('draft','publish')
	);
	$myQuery->query($param);
?>
<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
	<?php the_content();?>
<?php endwhile;endif;wp_reset_postdata();?>

<?php restore_current_blog();get_footer(); ?>
