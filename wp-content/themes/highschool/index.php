<?php get_header(); ?>
<section class="mv">
	<div class="slide">
		<div class="lead"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mv_lead.png" alt="覚えるより、 考える 。浅く知るより、 深く 理解する。正解を探す力よりも、自分で答えを創りだす力を手に入れよう。" width="467"></div>
		<ul>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_tankyu_slide_01.png" alt=""></li>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_tankyu_slide_02.png" alt=""></li>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_tankyu_slide_03.png" alt=""></li>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_tankyu_slide_04.png" alt=""></li>
		</ul>
		<?php if($text_below_mv = get_field('text_below_mv','options')):?>
		<div class="lead2"><?php echo $text_below_mv;?></div>
		<?php endif;?>
		<?php
			$date1 = get_field('info_date1','options');
			$sep = get_field('info_sep','options');
			$date2 = get_field('info_date2','options');
			if($date1 && $sep && $date2){
				$modf = 'all';
			}else{
				$modf = '';
			}

			$info_url = get_field('info_url','options');
		?>
		<aside class="date<?php if(!$info_url) echo ' nolink';?>">
			<?php if($info_url) echo '<a href="'.$info_url.'">';?>
				<div class="text1"><?php echo get_field('info_txt','options');?></div>
				<div class="text2 <?php echo $modf;?>">
					<?php
						if($date1) echo '<span class="date1">'.$date1.'</span>';
						if($sep) echo '<span class="sep">'.$sep.'</span>';
						if($date2) echo '<span class="date2">'.$date2.'</span> ';
					?>
				</div>
			<?php if($info_url) echo '</a>';?>
		</aside>
	</div>
</section>


<div class="backgrond-color--lightblue">
	<div class="backgrond-color__inner">
		<div class="top_about">
			<div class="col"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_about_img_left.png" width="531" alt="探究スクールとは 小・中・高 対象 入試対策も！ 国・数・英 教科学習 ゼミワークショップ 探究学習"></div>
			<div class="col"><a href="<?php echo home_url('/sotsugyo/')?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_about_img_right_off.png" width="388" alt="卒業生より 卒業生のインタビューを読む なっちゃん かっちゃん 勉強を楽しめるようになった。留学や海外の仕事を「やってみよう」と思えるようになった。" class="rollover"></a></div>
		</div>
	</div>
</div>

<div class="backgrond-color--lightgreen top_outlne">
<div class="backgrond-color__inner">
<?php if(have_rows('ol_each_info','options')): ?>
<?php
while(have_rows('ol_each_info','options')): the_row();

	switch (get_sub_field('ol_info_color')) {
		case '緑':
			$area_class ='border-area--green';
			$ttl_img1 = 'border--green--head.png';
			$ttl_img2 = 'border--green--head--sp.png';
			break;
		case '青':
			$area_class ='border-area--blue';
			$ttl_img1 = 'border--blue--head.png';
			$ttl_img2 = 'border--blue--head--sp.png';
			break;
		default:
			$area_class ='border-area--yellow';
			$ttl_img1 = 'border--yellow--head.png';
			$ttl_img2 = 'border--yellow--head--sp.png';
			break;
	}

	if(!get_sub_field('ol_info_link')){
		$area_class .= ' nolink';
	}
?>
	<section class="border-area <?php echo $area_class?>">
		<div class="border_header">
		<img class="pc" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/<?php echo $ttl_img1;?>" alt="">
		<img class="sp" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/<?php echo $ttl_img2;?>" alt="">
			<div class="ttl_area_wrapper sp">
				<div class="ttl_area">
					<div class="ttl_prefix">
						<div class="inner">
							<p><?php echo get_sub_field('ol_info_grade');?></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="border-body">
		<a class="border-body_inner" href="<?php echo get_sub_field('ol_info_link');?>">
			<div class="sep-sp sp"></div>
			<div class="ttl_area_wrapper">
				<header class="ttl_area">
					<div class="ttl_prefix pc">
						<div class="inner">
							<p><?php echo get_sub_field('ol_info_grade');?></p>
						</div>
					</div>
					<div class="ttl_text">
						<h3 class="ttl"><?php echo get_sub_field('ol_info_lead');?></h3>
						<div class="lead">
							<p><?php echo get_sub_field('ol_info_lead2');?></p>
						</div>
					</div>
				</header>
			</div>
			<div class="content_area_wrapper">
				<div class="row">
					<?php
						$lc = 0;
						$lcc = 1;
						$total = count(get_sub_field('ol_info_details'));
						if( have_rows('ol_info_details') ):
						while ( have_rows('ol_info_details') ) : the_row();

						if(get_sub_field('ol_details_content')){
							$modf = 'block--oneliner';
						}else{
							$modf = '';
						}
					?>
					<div class="block <?php echo $modf;?>">
						<div class="block__inner-1st">
							<div class="block__base_height">
								<div></div>
							</div>
							<div class="block__inner-2nd">
								<div class="above"><?php echo the_sub_field('ol_details_title');?></div>
								<?php if(get_sub_field('ol_details_content')):?>
									<div class="sep">&nbsp;</div>
									<div class="below"><?php echo the_sub_field('ol_details_content')?></div>
								<?php endif;?>
							</div>
						</div>
					</div>
					<?php
					if($lc == 1){
						if($total == $lcc){
							echo '</div>';
						}else{
							echo '</div><div class="row">';
						}
						$lc = 0;
					}else{
						$lc++;
					}
					$lcc++;
					endwhile;endif;
					if($lc === 1 && $total % 2 !== 0) echo '</div>';
					?>
			</div>
			</a>
		</div>
		<div class="border-hover">
			<div class="inner-1st">
				<div class="inner-2nd">
					<p class="ttl_1"><?php echo get_sub_field('ol_info_title');?></p>
					<p class="ttl_2">入会案内へ</p>
					<p class="icon"></p>
				</div>
			</div>
		</div>
		<?php
			$ol_info_images = get_sub_field('ol_info_images');
			if($ol_info_images){
				$ol_info_images = wp_get_attachment_image_src( $ol_info_images, 'full');
				$width = (int)$ol_info_images[1] / 2;
				echo '<div class="inview inview--to_bottom">';
				echo '<img src="'.$ol_info_images[0].'" alt="" width="'.$width.'">';
				echo '</div>';
			}
		?>
	</section>
	<?php endwhile; ?>
	<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
