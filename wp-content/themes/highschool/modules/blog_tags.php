<?php 
global $post;
global  $mmcf;
$terms = $mmcf->get_the_terms('category_blog',1,$post->ID);
$tags = array();
$tag_excludes = array('小学生','中学生','高校生','トップページに表示させる');
if($terms){
	echo '<aside class="tag_wrap tag_wrap--blog">';
	foreach($terms as $term){ 
		if(!in_array($term->name, $tag_excludes)){
			$term_color = get_field('cat_color','category_blog_'.$term->term_id);
			if(!$term_color){
				 $term_color = '#58b0a8';
			}
			$tags[] = '<span style="background-color:'.$term_color.';" class="tags" href="/'.$post_type.'/category/'.$term->slug.'">'.$term->name.'</span>';
		}
	}
	echo implode('<span class="sep"></span>', $tags);
	echo '</aside>';
}
?>