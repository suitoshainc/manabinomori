<?php global $mmcf;
if($query->have_posts()):
?>
	<?php if($single):?>
	<section class="blog_list blog_list--lower blog--relate">
	<div class="relate_img_1">
		<img src="<?php echo $this_theme_dir;?>/assets/images/img_blog_relate.png" alt="" class="relate_img" width="248">
	</div>
	<?php endif;?>
	<ul class="list">
		<?php
		while($query->have_posts()) : $query->the_post(); 
		if ( has_post_thumbnail() ){
			$imgid = get_post_thumbnail_id();
			$img = wp_get_attachment_image_src( $imgid, 'photo280-9999', $icon );
			$src = $img[0];
		}elseif($path = $mmcf->getPostFirstImg()){
			$src = $path;
		}else{
			$src = get_stylesheet_directory_uri().'/assets/images/img_blog_default.png';
		}
		?>
			<li><a href="<?php echo $mmcf->url(get_permalink(),'blog');?>" class="noanim">
				<div class="img" style="background:url(<?php echo $src;?>) no-repeat center center;background-size:280px auto;">
					<div class="inner"></div>
					<div class="filter"></div>
				</div>
				<div class="txt">
					<h3 class="ttl"><?php the_title();?></h3>
					<div class="pc">
						<?php if(!$layout_simple):?>
							<?php get_template_part('modules/blog_tags');?>
							<div class="desc">
								<p><?php echo winexcerpt(56);?></p>
							</div>
						<?php endif;?>
					</div>
				</div></a>
			</li>
		<?php endwhile; ?>
	</ul>
	<?php if($single):?>
	</section>
	<?php endif;?>
<?php endif;?>