<?php get_header();switch_to_blog(1); global $mmcf;?>
<div class="teacher">
	<header class="lower_header lower_header--teacher">
		<h1 class="ttl"><span>先生たち</span></h1>
	</header>
	<div class="backgrond-color--lightblue nosep">
	<div class="t_data_wrapper">
		<div class="inner">
			<?php
				$myQuery = new WP_Query();
				$param = array(
					'posts_per_page' => -1,
					'post_type' => 'teacher_data'
				);
				$myQuery->query($param);
			?>
			<?php
			$lc = 1;
			if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post();
			$t_pre = get_field('t_pre');
			$t_photo = get_field('t_photo');
			$t_hobby = get_field('t_hobby');
			$t_other = get_field('t_other');
			$t_his = get_field('t_his');
			$t_msg = get_field('t_msg');

			$t_photo = wp_get_attachment_image_src( $t_photo, 'full');
			$width = (int)$t_photo[1] / 2;

			switch ($lc) {
				case '1':
					$c = 'odd';
					break;
				case '2':
					$c = 'even';
					break;
				case '3':
					$c = 'odd';
					break;
				default:
					# code...
					break;
			}
			?>
				<div class="card--teacher">
				<div class="img">
					<img src="<?php echo $t_photo[0];?>" alt="" width="<?php echo $width;?>">
					<a class="btn execColorBox nosms" href="#target_<?php echo $post->ID;?>">message</a></div>
					<div class="content <?php echo $c;?>">
						<h2 class="ttl"><span class="sub"><?php echo $t_pre;?></span><span class="main"><?php the_title();?></span></h2>
						<div class="info">
							<div class="top">
								<div class="th">趣味：</div>
								<div class="td">
									<p><?php echo $t_hobby;?></p>
								</div>
							</div>
							<div class="other1">
								<p>好きな...<br></p>
								<?php echo $t_other;?>
							</div>
							<div class="other2">
								<p>
									<?php echo $t_his;?>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="div" style="display:none;">
					<div class="colorBoxTarget" id="target_<?php echo $post->ID;?>">
						<header class="header">
							<div class="img">
							<img src="<?php echo $t_photo[0];?>" alt="" width="<?php echo $width;?>">
							</div>
							<div class="info">
								<h2 class="ttl"><span class="sub"><?php echo $t_pre;?></span><span class="main"><?php the_title();?></span></h2>
							</div>
						</header>
						<div class="msg"><?php echo $t_msg;?></div>
					</div>
				</div>
			<?php
			if($lc == 3) {
				$lc = 1;
			}else{
				$lc++;
			}
			endwhile;endif;wp_reset_postdata();?>
		</div>
	</div>
	</div>
</div>
<?php restore_current_blog();get_footer(); ?>
