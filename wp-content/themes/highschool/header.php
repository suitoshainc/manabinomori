<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="shortcut icon" href="<?php echo home_url('/favicon.ico')?>" />
	<?php if(wpmd_is_tablet()):?>
	<meta id="viewport" content="width=1400" name="viewport">
	<?php else:?>
	<meta id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,user-scalable=1" name="viewport">
	<?php endif;?>
	<?php wp_head(); ?>

	<?php 
	global $wp_query;
	$pt = $wp_query->query_vars['mm_post_type'];
	if($pt || (!is_home() && !is_page('manabi1'))):
	?>
		<script>
			var GROBAL_WAVE_BACKGROUND = '#EEF6F8';
		</script>
	<?php else: ?>
		<script>
			var GROBAL_WAVE_BACKGROUND = '#f5fbef';
		</script>
	<?php endif;?>
</head>
<body id="top" <?php echo body_class();?>>
	<div id="wrap">
		<nav class="spnavi sp" id="spnavi">
			<div id="scroller">
				<ul>
					<?php get_template_part('modules/gnav_1');?>
					<?php get_template_part('modules/gnav_2');?>
				</ul>
			</div>
		</nav>
		<div class="btn_sp_navi_wrap"><a class="btn_sp_navi nosc nosms" href="#sp"><span class="bar"></span><span class="bar"></span><span class="bar"></span><span class="text">MENU</span></a></div>
		<header class="main_header">
			<div class="inner">
				<a href="<?php echo home_url();?>"><h1 class="logo" title="学びの森 ハイスクール"><span>学びの森 ハイスクール</span></h1></a>
				<aside class="min60"><a href="<?php echo home_url('contact/');?>">60分無料相談</a></aside>
				<aside class="tel"><a class="tel_link" href="tel:0771-29-5800">TEL :<span>0771-29-5588</span></a></aside>
			</div>
			<div class="leaf1 leaf"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_header--leaf_1.png" alt=""></div>
			<div class="leaf2 leaf"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_header--leaf_2.png" alt=""></div>
		</header>
		<div class="gnav_wrap">
			<div class="inner">
				<nav class="gnav">
					<ul>
						<?php get_template_part('modules/gnav_1');?>
						<?php get_template_part('modules/gnav_2');?>
					</ul>
				</nav>
			</div>
		</div>