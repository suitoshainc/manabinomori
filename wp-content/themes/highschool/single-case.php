<?php global $mmcf; get_header(); ?>
<header class="lower_header lower_header--case">
	<h1 class="ttl"><span>子どもたちの変容</span></h1>
</header>
<div class="backgrond-color--lightblue nosep">
	<?php 
	$single = true;
	$case_top = home_url('case');
	$num = $mmcf->return_case_num();
	include( locate_template('modules/case_archive_lead.php', false, false ) );?>
	<article class="content_area content_area--case">
	<?php switch_to_blog(1);?>
		<?php 
		$p = $mmcf->get_post();
		if($p->have_posts()): while($p->have_posts()) : $p->the_post(); 
		$pattern = get_field('case_pattern');
		if($pattern){
			$pattern_class = 'bg_p_l'.$pattern;
		}else{
			$pattern_class = 'bg_p_l1';
		}
		?>
		<header class="main_ttl_wrapper">
			<div class="main_ttl <?php echo $pattern_class;?>">
				<h2 class="ttl"><span><?php the_title();?></span></h2>
				<div class="num">NO.<?php echo sprintf('%03d', $num[$post->ID]);?></div>
			</div>
		</header>
		<div class="inner">
			<section class="main">
				<?php 
					$rows = get_field('case_contents');
					$row_count = count($rows);
					$lc = 0;
					if( have_rows('case_contents') ):
					while ( have_rows('case_contents') ) : the_row();
					$lc++;
				?>
					<h3 class="ttl"><?php the_sub_field('case_contents_ttl');?></h3>
					<div class="txt">
						<?php the_sub_field('case_contents_content');?>
					</div>
				<?php 
					
					if($lc !== $row_count){
						echo '<div class="sep"></div>';
					}
					endwhile;
					else :
					// no rows found
					endif;
				?>
			</section>
			<section class="sub">
				<div class="meta">
					<?php 
					if($case_gender = get_field('case_gender')){
						echo '<p>性別 : '.$case_gender.'</p>';
					}
					if($case_age = get_field('case_age')){
						echo '<p>在籍年齢 : '.$case_age.'</p>';
					}
					?>
				</div>
				
					<?php 
						if( have_rows('case_photo') ):
							echo '<div class="photos">';
						while ( have_rows('case_photo') ) : the_row();
						$photoid = get_sub_field('case_photo_data');
						$thumb = wp_get_attachment_image_src($photoid , 'photo340-9999' );
						if($thumb):
					?>
						<div class="thumb">
							<img src="<?php echo $thumb[0];?>" alt="">
						</div>
					<?php 
						endif;
						endwhile;
						echo '</div>';
						else :
						// no rows found
						endif;
					?>
				</section>
		</div>
		<aside class="post_meta">
			<?php get_template_part('modules/case_tags');?>
			<p class="date"><?php the_time('Y / m / d')?></p>
		</aside>
		<?php endwhile;endif; wp_reset_postdata();?>
	<?php restore_current_blog(); ?>
	</article>
</div>
<?php get_footer(); ?>
