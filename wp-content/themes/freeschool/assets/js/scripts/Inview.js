const livTweenMax = require('../../lib/js/TweenMax/TweenMax.min.js');
const libInview = require('../../lib/js/jquery.inview/jquery.inview.min.js');
const libImgagesLOaded = require('../../lib/js/imagesloaded/imagesloaded.pkgd.min.js');
const libEasePack = require('../../lib/js/easing/EasePack.min.js');

class Inview{
	animationToBottom(elem,complete_elem,speed = 0.5,rate = 20,delay = 0){
		let tw = TweenMax.to($(elem) , speed , {
			top:rate,
			autoAlpha :1,
			delay : delay,
			ease : "swing",
			onComplete:function(){
				$(complete_elem).addClass('inview_fin');
			}
		});

		return tw;
	};

	animationToTop(elem,complete_elem,speed = 0.5,rate = 20,delay = 0,bounce = true){
		let param = {
			top:rate,
			autoAlpha :1,
			delay : delay,
			ease: Bounce.easeOut,
			onComplete:function(){
				$(complete_elem).addClass('inview_fin');
			}
		};
console.log(bounce);
		if(bounce == false){
			param.ease = "swing";
		}
		let tw = TweenMax.to($(elem) , speed , param);

		return tw;
	};

	load(target,type,speed = 0.5,rate = 20,delay = 0,bounce = true){
		const self = this;
		$('body').imagesLoaded(function(){
			$(' > *',target).on('inview',function(event, isInView, visiblePartX, visiblePartY){
				if(!$(this).hasClass('inview_fin')){
					switch (type) {
						case 'toBottom':
							self.animationToBottom($(this),$(this),speed,rate,delay);
							break;
						case 'toTop':
							self.animationToTop($(this),$(this),speed,rate,delay,bounce);
							break;
					}
				}
			});
		});
	};
}
module.exports = Inview;


