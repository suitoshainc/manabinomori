<?php get_header(); global $mmcf;?>
<section class="mv">
	<div class="slide">
		<ul>
		<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/slide1.png" alt=""></li>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/slide2.png" alt=""></li>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/slide3.png" alt=""></li>
			<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/slide4.png" alt=""></li>
		</ul>
	<div class="inner">
		<div class="lead"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mv_lead.png" alt="じっくり向き合う のびのび学ぶ自分に 人に 出会うそして 一歩ずつ 前へ" width="440"></div>
			<aside class="date_info">
				<?php
					switch_to_blog(1);
					$myQuery = new WP_Query();
					$param = array(
						'posts_per_page' => 1,
						'post_type' => 'blog',
						'tax_query' => array(
							array(
								'taxonomy'=>'category_blog',
								'terms'=> 'show_top',
								'field'=>'slug'
							)
						)
					);
					$myQuery->query($param);
				?>
				<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
					<a href="<?php echo $mmcf->url(get_permalink(),'blog');?>">
						<div class="text1" style="height:auto;"><?php the_title();?></div>
					</a>
				<?php endwhile; ?>
				<?php else:?>
					<a href="/qa">
						<div class="text1" style="top: 130px;">不登校Q&amp;A</div>
					</a>
				<?php endif;wp_reset_postdata();restore_current_blog();?>
			</aside>
		</div>
	</div>
</section>
<section class="backgrond-color--lightblue">
	<div class="backgrond-color__inner">
		<?php echo get_field('top_first','options');?>
		<!-- <div class="top_links">
			<a class="noanim" href="/sotsugyo1"><img class="rollover" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_gr_off.png" width="502" alt="卒業生より めぐみちゃん  「学校や大人同士の話よりも、まずはめぐみちゃんを中心に考えましょう」という言葉に救われました。"></a>
			<a class="noanim link2" href="/sotsugyo2"><img class="rollover" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_gr_2_off.png" width="534" alt="卒業生より ルイくん 実家みたいに思っちゃってます。ルイの高校受験のために東京まで行ってくださいましたよね。"></a>

			<div class="img1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_top_support.png" width="319" alt="小・中学生対象 京都府教育委員会認定フリースクールとは...教育委員会の認定により、学びの森への出席が在籍校への出席となります。通学定期の購入可。">
				<a href="http://high.manabinomori.co.jp/" target="_blank" class="noanim no_icon">
					<img class="rollover" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_free_off.png" width="230" alt="" class="高校生の方はこちら 高校生対象通信制サポート校">
				</a>
			</div>
			
			<div class="btn_wrap">
				<a class="img2 noanim" href="/blog/category/cat5"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_media_off.png" width="163" alt="メディア掲載" class="rollover"></a>
				<a class="img3 noanim" href="/movie#cat4"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_rec_off.png" width="161" alt="推薦者の声" class="rollover"></a>
			</div>
			<p class="lead"><img class="pc am" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_top_outline_lead.png" width="742" alt="不登校になったからこそ、もっと自由な学びを。もっと豊かな人生を。">
				<img class="sp am" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_top_outline_lead_sp.png" alt="不登校になったからこそ、もっと自由な学びを。もっと豊かな人生を。"></p>
		</div> -->
	</div>
</section>

<section class="backgrond-color--lightgreen">
	<div class="backgrond-color__inner">
		<?php echo get_field('top_second','options');?>
		<!-- <section class="top_outline">
			<h2 class="ttl ac"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ttl_method.png" width="73" alt="学び方"></h2>
			<div class="left">
				<section class="border-area border-area--yellow border-area--s">
				<div class="border_header"><img class="pc" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/border--yellow--head-s.png" alt=""><img class="sp" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/border--yellow--head--sp.png" alt="">
						<div class="ttl_area_wrapper sp">
							<div class="ttl_area">
								<div class="ttl_prefix">
									<div class="inner">
										<p>小学生</p>
										<div class="icon">週4回 10:30~16:00</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="border-body">
						<div class="border-body_inner">
							<div class="sep-sp sp"></div>
							<div class="ttl_area_wrapper">
								<header class="ttl_area">
									<h3 class="ttl_prefix pc">
										<div class="ttl">小学生</div>
										<div class="icon">週4回 10:30~16:00</div>
									</h3>
								</header>
							</div>
							<div class="content_area_wrapper">
								<div class="inner">
									<p>
										・基礎学力の回復<br>
										・個別学習、探究学習、体験活動<br>
										・中学受験対策・進路指導<br>
										・生活面・心理面サポート
									</p>
									<table class="add">
										<tr>
											<th>進路実績</th>
											<td>大谷中学、京都光華女子中学、<br>花園中学、京都学園中学など</td>
										</tr>
									</table>
								</div><a class="btn" href="/manabi1">詳しく見る</a>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="right">
				<section class="border-area border-area--green border-area--s">
					<div class="border_header"><img class="pc" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/border--green--head-s.png" alt=""><img class="sp" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/border--green--head--sp.png" alt="">
						<div class="ttl_area_wrapper sp">
							<div class="ttl_area">
								<div class="ttl_prefix">
									<div class="inner">
										<p>中学生</p>
										<div class="icon">週4回 10:30~16:00</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="border-body">
						<div class="border-body_inner">
							<div class="sep-sp sp"></div>
							<div class="ttl_area_wrapper">
								<header class="ttl_area">
									<h3 class="ttl_prefix pc">
										<div class="ttl">中学生</div>
										<div class="icon">週4回 10:30~16:00</div>
									</h3>
								</header>
							</div>
							<div class="content_area_wrapper">
								<div class="inner">
									<p>
										・基礎学力の回復<br>
										・個別学習、探究学習、体験活動<br>
										・高校受験対策・進路指導<br>
										・生活面・心理面サポート
									</p>
									<table class="add">
										<tr>
											<th>進路実績</th>
											<td>公立高校、京都外大西高校、花園高校など</td>
										</tr>
									</table>
								</div><a class="btn" href="/manabi2">詳しく見る</a>
							</div>
						</div>
					</div>
				</section>
			</div>
		</section>

		<section class="top_tt">
			<h2 class="ttl"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ttl_top_manabi.png" width="148" alt="ある日の学び"></h2><a class="noanim" href="/aruhi"><img class="rollover" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_manabi_off.png" alt="" width="870"></a>
		</section> -->
		<section class="blog_list">
		<h2 class="ttl ac"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ttl_top_henyo.png" width="195" alt="子どもたちの変容"></h2>
			<p class="more pc"><a href="/case">&gt;もっと見る</a></p>
			<ul class="list">
				<?php 
				switch_to_blog(1);
				$param = array(
					'posts_per_page' => 3,
					'post_type' => 'case'
				);
				$num = $mmcf->return_case_num();
				$query = $mmcf->get_post($param,false);
				if($query->have_posts()): while($query->have_posts()) : $query->the_post(); 
					$pattern = get_field('case_pattern');
					if($pattern){
						$pattern_class = 'bg_p'.$pattern;
					}else{
						$pattern_class = 'bg_p1';
					}
					$titles = array();
					if( have_rows('case_contents') ){
						while ( have_rows('case_contents') ) {
							the_row();
							$titles[] = strip_tags(get_sub_field('case_contents_ttl'));
						}
					} 
				?>
				<li><a href="<?php echo $mmcf->url(get_permalink(),'case');?>" class="no_anim">
					<div class="img <?php echo $pattern_class;?>">
						<div class="inner"></div>
						<div class="num">NO.<?php echo sprintf('%03d', $num[$post->ID]);?></div>
						<div class="filter"></div>
					</div>
					<div class="txt">
						<h3 class="ttl"><?php the_title();?></h3>
					</div></a>
				</li>
				<?php endwhile;endif; wp_reset_postdata();restore_current_blog();?>
			</ul>
			<p class="more sp"><a href="/case">&gt;もっと見る</a></p>
		</section>
		<section class="blog_list">
			<h2 class="ttl ac"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ttl_top_blog.png" width="44" alt="日誌"></h2>
			<p class="more pc"><a href="/blog">&gt;もっと見る</a></p>
			<ul class="list list--diary">
			<?php 
			switch_to_blog(1);
			$param = array(
				'posts_per_page' => 3,
				'post_type' => 'blog',
				'tax_query' => array(
					array(
						'taxonomy'=>'category_blog',
						'terms'=>array( 'cat2','cat1'),
						'field'=>'slug',
					)
				)
			);
			
			$query = $mmcf->get_post($param,false);
			if($query->have_posts()): while($query->have_posts()) : $query->the_post(); 
				if ( has_post_thumbnail() ){
					$imgid = get_post_thumbnail_id();
					$img = wp_get_attachment_image_src( $imgid, 'photo280-9999', $icon );
					$src = $img[0];
				}elseif($path = $mmcf->getPostFirstImg()){
					$src = $path;
				}else{
					$src = get_stylesheet_directory_uri().'/assets/images/img_blog_default.png';
				}
			?>
				<li>
				<a href="<?php echo $mmcf->url(get_permalink(),'blog');?>" class="noanim">
					<div class="img" style="background:url(<?php echo $src;?>) no-repeat center center;background-size:280px auto;">
						<div class="inner"></div>
						<div class="filter"></div>
					</div>
					<div class="txt"><span class="date"><?php the_time('Y / m / d');?></span>
					<p><?php the_title();?></p>
					</div>
				</a>
				</li>
			<?php endwhile;endif; wp_reset_postdata();restore_current_blog();?>
			</ul>
			<p class="more sp"><a href="/blog">&gt;もっと見る</a></p>
		</section>
	</div>
</section>
<?php restore_current_blog(); ?>
<?php get_footer(); ?>
