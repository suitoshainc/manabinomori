<?php 
get_header(); 
global $mmcf;
global $post; 
global $wp_query;
$case_top = home_url('case');
switch_to_blog(1);
$mm_term = $wp_query->query_vars['mm_term'];
$mm_taxonomy = $wp_query->query_vars['mm_taxonomy'];
?>
<header class="lower_header lower_header--case">
	<h1 class="ttl"><span>子どもたちの変容</span></h1>
</header>
<div class="backgrond-color--lightblue nosep">
	<?php include( locate_template('modules/case_archive_lead.php', false, false ) );?>
	<section class="blog_list blog_list--lower">
		<?php 
		$param = array(
			'posts_per_page' => 12
		);
		//paged
		if($paged = $wp_query->query_vars['paged']){
			$param['paged'] = $paged;
		}
		//tax query
		if($mm_term && $mm_taxonomy){
			$param['tax_query'] = array(
				array(
					'taxonomy' => $mm_taxonomy,
					'terms'=> $mm_term,
					'field'=> 'slug'
				)
			);
		}
		$query = $mmcf->get_post($param,false);
		
		include( locate_template('modules/case_archive_loop.php', false, false ) );
		
		if(function_exists('wp_pagenavi')) {wp_pagenavi(array('query' => $query ));}
			wp_reset_postdata();
		?>
	</section>
</div>
<?php restore_current_blog(); ?>
<?php get_footer(); ?>
