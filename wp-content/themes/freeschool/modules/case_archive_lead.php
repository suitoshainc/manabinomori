<?php 
global $mmcf;global $post; 
$req = $wp_query->query_vars['mm_term'];
?>
<?php if(!$single):?>
<div class="lead lead--case">
	<p>私たちは、学びの森での子どもたちの変容を”蝶のサナギ”に例えます。<br>
まるで全てが止まっているかのように見えるサナギの中で<br>
それまでの姿は溶けてなくなり、新しい細胞が生まれます。<br>
私たちがここで見届けるのは、子どもたちのサナギの期間。<br>
やがて皆、新たな自分へと生まれ変わり、学びの森を卒業していきます。</p>
</div>
<?php endif;?>
<aside class="term_list">
	<ul>
		<li><a href="<?php echo $case_top;?>">ALL</a></li>
		<?php 
		$terms = $mmcf->get_terms('category_case',1);
		foreach($mmcf->get_terms('category_case',1) as $term):
			if($req){
				if($req == $term->slug){
					$active = true;
				}else{
					$active = false;
				}
			}
		?>
		<li<?php if($active) echo ' class="active"';?>><a href="/<?php echo $term->post_type;?>/category/<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>
		<?php endforeach;?>
	</ul>
</aside>