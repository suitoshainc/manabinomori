<?php global $mmcf;?>
<ul class="list">
	<?php
	$num = $mmcf->return_case_num();
	if($query->have_posts()): while($query->have_posts()) : $query->the_post(); 
		$pattern = get_field('case_pattern');
		if($pattern){
			$pattern_class = 'bg_p'.$pattern;
		}else{
			$pattern_class = 'bg_p1';
		}
		$titles = array();
		if( have_rows('case_contents') ){
			while ( have_rows('case_contents') ) {
				the_row();
				$titles[] = strip_tags(get_sub_field('case_contents_ttl'));
			}
		} 
	?>
		<li><a href="<?php echo $mmcf->url(get_permalink(),'case');?>" class="no_anim">
			<div class="img <?php echo $pattern_class;?>">
				<div class="inner"></div>
				<div class="num">NO.<?php echo sprintf('%03d', $num[$post->ID]);?></div>
				<div class="filter"></div>
			</div>
			<div class="txt">
				<h3 class="ttl"><?php the_title();?></h3>
				<?php get_template_part('modules/case_tags');?>
				<div class="desc">
					<p><?php echo implode('/', $titles);?></p>
				</div>
			</div></a>
		</li>
	<?php endwhile;endif; ?>
</ul>