<?php 
global $wp_query;
$req = $wp_query->query_vars['category_qa']
?>
<header class="lower_header lower_header--qa">
	<h1 class="ttl"><span>不登校Q&amp;A</span></h1>
</header>
<div class="backgrond-color--lightblue nosep qa_list--wrap">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/imgf_qa.png" alt="" class="aside_img" width="330">
	<div class="lead lead--case">
		<p>不登校で悩んだ時、まずは、お子さんも親御さんも視点を少し広げて<br>
自分たちの状況を冷静に見つめ直していただきたいです。<br>
真剣に考えれば考える程、視野がせまくなって自分や相手を責めてしまいがち。<br>
安心してください。不登校は、お子さんにとっても親御さんにとっても、<br>
新たな人生へのはじまりになる経験です。</p>
	</div>
	<aside class="term_list term_list--tab">
		<ul>
			<li<?php if(!$req) echo ' class="active"';?>><a href="<?php echo home_url('qa');?>" class="nosms">ALL</a></li>
			<?php 
			$terms = get_terms('category_qa');
			$new_terms = array();
			$active = false;
			foreach($terms as $term):
				if($req){
					if($req == $term->slug){
						$new_terms[] = $term;
						$active = true;
					}else{
						$active = false;
					}
				}
			?>
				<li class="tab_controller <?php if($active) echo 'active';?>"><a href="<?php echo get_term_link($term);?>" class="nosms"><?php echo $term->name;?></a></li>
			<?php endforeach;?>
		</ul>
	</aside>
	<?php if($new_terms):?>
		<?php foreach($new_terms as $term):?>
			<section id="<?php echo $term->slug;?>" class="wrap--list-qa">
				<?php
					$myQuery = new WP_Query();
					$param = array(
						'post_type' => 'qa',
						'posts_per_page' => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'category_qa',
								'terms'=> $term->slug,
								'field'=> 'slug'
							)
						)
					);

					$myQuery->query($param);
					$lc = 1;
				?>
				<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
					<dl class="list-qa">
						<dt class="ttlarea">
							<h3 class="inner">
								<div class="num">Q <?php echo $lc;?></div>
								<div class="ttl"><?php the_title();?></div>
							</h3>
						</dt>
						<dd class="txt"><?php the_content();?></dd>
					</dl>
				<?php $lc++;endwhile;endif;wp_reset_postdata();?>
			</section>
		<?php endforeach;?>
	<?php else:?>
		<section class="wrap--list-qa">
			<?php
				$myQuery = new WP_Query();
				$param = array(
					'post_type' => 'qa',
					'posts_per_page' => -1,
				);

				$myQuery->query($param);
				$lc = 1;
			?>
			<?php if($myQuery->have_posts()): while($myQuery->have_posts()) : $myQuery->the_post(); ?>
				<dl class="list-qa">
					<dt class="ttlarea">
						<h3 class="inner">
							<div class="num">Q <?php echo $lc;?></div>
							<div class="ttl"><?php the_title();?></div>
						</h3>
					</dt>
					<dd class="txt"><?php the_content();?></dd>
				</dl>
			<?php $lc++;endwhile;endif;wp_reset_postdata();?>
		</section>
	<?php endif;?>
	<p class="ac btn">
		<a href="/contact"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_qa.png" alt="どなたでもお気軽にご連絡ください。 60分無料相談 TEL : 0771-29-5588"></a>
	</p>
</div>