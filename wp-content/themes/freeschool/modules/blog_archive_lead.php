<?php 
global $mmcf;global $post; 
$req = $wp_query->query_vars['mm_term'];
?>
<aside class="term_list">
	<ul>
		<li><a href="<?php echo $blog_top;?>">ALL</a></li>
		<?php 
		$tag_excludes = array('小学生','中学生','高校生','トップページに表示させる');
		$terms = $mmcf->get_terms('category_blog',1,true,array('高校生'),array('小学生','中学生'));
		if($terms):
		foreach($terms as $term):
		if(!in_array($term->name, $tag_excludes)):
			if($req){
				if($req == $term->slug){
					$active = true;
				}else{
					$active = false;
				}
			}
		?>
		<li<?php if($active) echo ' class="active"';?>><a href="/<?php echo $term->post_type;?>/category/<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>
		<?php endif;endforeach;endif;?>
	</ul>
</aside>