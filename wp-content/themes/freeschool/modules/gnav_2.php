<li class="nav_4"><a href="<?php echo home_url('hibi/');?>"><span>日々のようす</span></a>
	<ul class="child">
		<li class="c_nav_1"><a href="<?php echo home_url('aruhi/');?>"><span>ある日の学び</span></a></li>
		<li class="c_nav_2"><a href="<?php echo home_url('teacher/');?>"><span>先生たち</span></a></li>
		<li class="c_nav_3"><a href="<?php echo home_url('movie/');?>"><span>movie</span></a></li>
	</ul>
</li>
<li class="nav_5"><a href="<?php echo home_url('case/');?>"><span>子どもたちの変容</span></a></li>
<li class="nav_6"><a href="<?php echo home_url('qa/');?>"><span>不登校Q&amp;A</span></a></li>
<li class="nav_7"><a href="<?php echo home_url('access/');?>"><span>アクセスと概要</span></a></li>
<li class="nav_8"><a href="<?php echo home_url('blog/');?>"><span>日誌</span></a></li>
<li class="nav_9"><a href="<?php echo home_url('contact/');?>"><span>お問い合わせ</span></a></li>
<li class="fb"><a class="no_icon" href="https://www.facebook.com/free.manabinomori.co.jp/" target="_blank"><span>Facebook</span></a></li>