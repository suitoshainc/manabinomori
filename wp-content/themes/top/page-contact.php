<?php get_header(); ?>
<header class="main_header">
	<h1 class="logo" title="学びの森"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-2.png" alt="学びの森" width="340"></a></h1>
</header>
<section class="sub_header">
<?php echo do_shortcode('[contact-form-7 id="4" title="お問い合わせ"]')?>
</section>

<?php get_footer(); ?>
