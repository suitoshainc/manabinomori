<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="shortcut icon" href="<?php echo home_url('/favicon.ico')?>" />
	<?php if(wpmd_is_tablet()):?>
	<meta id="viewport" content="width=1200" name="viewport">
	<?php else:?>
	<meta id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,user-scalable=1" name="viewport">
	<?php endif;?>
	<?php wp_head(); ?>

	<script>
		var GROBAL_WAVE_BACKGROUND = '#F7F6F1';
	</script>
</head>
<body id="top" <?php echo body_class();?>>
	<div id="wrap">