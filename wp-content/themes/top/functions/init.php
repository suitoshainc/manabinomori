<?php
// metaやlinkの削除
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', '10');
remove_action('wp_head', 'start_post_rel_link', '10');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action( 'wp_head','wp_shortlink_wp_head',10, 0 );
remove_action('wp_head', 'feed_links_extra',3,0);

// headに含まれるインラインスタイルの削除
function remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'remove_recent_comments_style' );

//コンテンツフィルターからpタグの削除
remove_filter ('the_content', 'wpautop');
//抜粋からpタグの削除
remove_filter('the_excerpt', 'wpautop');

// //オリジナルの画像サイズを追加
add_image_size( 'photo195-9999', 195, 9999, true );
add_image_size( 'photo340-9999', 340, 9999, false );
add_image_size( 'photo280-9999', 280, 9999, false );

/* ===============================================
css,js読み込み
=============================================== */
function suitosha_scripts(){
	//stylesheet
	wp_enqueue_style( 'style', get_stylesheet_directory_uri().'/style.css' );
	wp_enqueue_style( 'awesome-css', get_stylesheet_directory_uri().'/assets/lib/css/font-awesome/font-awesome.min.css');
	wp_enqueue_style( 'style-css', get_stylesheet_directory_uri().'/assets/css/style.min.css');

	if(is_user_logged_in()){
		wp_enqueue_style( 'preview-support', get_stylesheet_directory_uri().'/assets/css/preview_support.css');
	}
	//javscript
	wp_enqueue_script('jquery');
	wp_enqueue_script('lodash', get_stylesheet_directory_uri().'/assets/lib/js/lodash/lodash.min.js', array('jquery'), '', true);
	wp_enqueue_script('bundle', get_stylesheet_directory_uri().'/assets/js/bundle.js', array('jquery'), '', true);

}
add_action( 'wp_enqueue_scripts', 'suitosha_scripts' );
?>
