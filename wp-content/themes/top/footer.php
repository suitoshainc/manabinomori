<footer id="footer">
	<div class="footer_img">
		<div class="top">
			<canvas id="wave1"></canvas>
		</div>
		<div class="bottom">
			<canvas id="wave2"></canvas>
		</div>
	</div>
	<div class="footer_info">
		<div class="inner">
			<div class="col1 col">
				<p>
					代表　北村 真也<br>
					〒621-0846<br>
					京都府亀岡市南つつじヶ丘大葉台2-44-9<br class="sp"><a class="map_link" href="https://goo.gl/maps/roiBVm4nw9S2" target="_blank"><span>MAP<br></span></a><a class="tel_link" href="tel:0771-29-5800">TEL: 0771-29-5800</a>
				</p>
			</div>
			<div class="col2 col"><a class="noicon" target="_blank" href="https://forest-manabinomori.amebaownd.com/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_footer_bn_1.png" alt="小さな森の実行委員会" width="147"></a><a href="http://www.kyoto-hikikomori-net.jp/kyoto/kizuna_suport.php" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_footer_bn_2.png" alt="京都府青少年課チーム絆" width="166"></a></div>
		</div>
	</div>
</footer>
<div id="responsive_flg"></div>
</div>
<?php if(is_page('contact')):?>
	<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
	<script>
	(function($) {
	$(function(){
		$('#zip').on('keyup',function(){
			AjaxZip3.zip2addr( 'zip', '', 'addr', 'addr');
		});
	});
	})(jQuery);

	</script>
<?php endif;?>
<?php wp_footer(); ?>
</html>
