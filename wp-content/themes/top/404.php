<?php get_header(); ?>
	<header class="main_header">
		<h1 class="logo" title="学びの森"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-2.png" alt="学びの森" width="340"></a></h1>
	</header>
	<div class="pb60">
		<p class="ac fs20 bold">お探しのページは存在しません。</p>
		<p class="ac fs18 bold mr15 pb50"><a href="/"><i class="fa fa-angle-right" aria-hidden="true"></i> トップページへ</a></p>
	</div>
<?php get_footer(); ?>
