<?php 
global $wp_query;
get_header(); 
$this_theme_dir = get_stylesheet_directory_uri();
?>
<header class="lower_header lower_header--blog">
	<h1 class="ttl"><span>日誌</span></h1>
</header>
<div class="backgrond-color--lightblue nosep">
	<article class="content_area content_area--blog">
		<?php 
		if (have_posts()) : while (have_posts()) : the_post();
		$base_id = $post->ID;
		?>
		<div class="main_ttl">
			<h2 class="ttl"><span><?php the_title();?></span></h2>
		</div>
		<div class="inner">
			<?php the_content();?>
			<aside class="post_meta">
				<?php include( locate_template('/modules/blog_tags.php', false, false ) );?>
				<p class="date"><?php the_time('Y / m / d')?></p>
			</aside>
		</div>
		<?php endwhile;endif; wp_reset_postdata();?>
		
	</article>
</div>
<?php get_footer(); ?>
