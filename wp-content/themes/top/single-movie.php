<?php 
get_header(); 
?>
<header class="lower_header lower_header--movie">
	<h1 class="ttl"><span>movie</span></h1>
</header>
<div class="backgrond-color--lightblue nosep movie_list--wrap">
<?php if (have_posts()) : ?>
<?php 
while (have_posts()) : the_post(); 
$terms = get_the_terms( $post, 'category_movie' );
?>
	<section class="blog_list blog_list--lower movie_list">
		<h2 class="main_ttl"><?php echo $terms[0]->name;?></h2>
		<ul class="list">
			<li>
				<div class="movie">
					<iframe width="300" height="168" src="https://www.youtube.com/embed/<?php echo get_field('youtube_link');?>?showinfo=0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="txt">
					<h3 class="ttl"><?php the_title();?></h3>
				</div>
			</li>
		</ul>
	</section>
<?php endwhile; ?>
<?php else : ?>
     <h2 class="title">記事が見つかりませんでした。</h2>
     <p>検索で見つかるかもしれません。</p><br />
     <?php get_search_form(); ?>
<?php endif; ?>
<?php get_footer(); ?>
