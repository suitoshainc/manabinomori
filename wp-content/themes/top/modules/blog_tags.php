<?php 
global $post;
$terms = get_the_terms($post->ID,'category_blog');
$tags = array();
if($terms){
	echo '<aside class="tag_wrap tag_wrap--blog">';
	foreach($terms as $term){ 
		if($term->name !== '高校生'){
			$term_color = get_field('cat_color','category_blog_'.$term->term_id);
			if(!$term_color){
				 $term_color = '#58b0a8';
			}
			$tags[] = '<span style="background-color:'.$term_color.';" class="tags" href="/'.$post_type.'/category/'.$term->slug.'">'.$term->name.'</span>';
		}
	}
	echo implode('<span class="sep"></span>', $tags);
	echo '</aside>';
}
?>