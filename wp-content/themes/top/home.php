<?php get_header(); ?>
<header class="main_header">
	<h1 class="logo" title="学びの森"><a href="<?php echo home_url();?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-2.png" alt="学びの森" width="340"></a></h1>
</header>
<section class="sub_header">
<a href="http://tanq.manabinomori.co.jp/" target="_blank">
<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_tankyu-2.png" alt="= 学習塾 = 学びの森 探求スクール" width="229"></a>
<span class="sp_sep">
	<a href="http://free.manabinomori.co.jp/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_freeschool-2.png" width="280" alt="= 京都府教育委員会認定フリースクール = 学びの森 フリースクール"></a>
	<a href="http://high.manabinomori.co.jp/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_top_highschool-2.png" width="231" alt="= 通信制サポート校 = 学びの森 ハイスクール"></a>
</span>

</section>

<p class="inquiry ac"><a href="<?php echo home_url('/contact/');?>" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/btn_to_inquiry.png" alt="お問い合わせ" width="416"></a></p>
<p class="ac txt"><img class="pc am" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_text_01.png" alt="みなさまに長年にわたり親しんでいただいた「アウラ」「知誠館」の名を2017年より 学びの森 に変更いたしました。森には様々な命が息づいています。ここでは、学校のように誰もが同じことをめざすわけではありません。一人ひとりの子どもたちがそれぞれのペースで学び、ワクワクしながら探究を続け、自分の人生を切り拓くための答えを創り出していく。そんな豊かな学びの世界をめざします。私たちは、学びの森のような場所が社会の中に増えていくことを願っています。" width="1092"><img class="sp" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_text_01_sp.png" alt="みなさまに長年にわたり親しんでいただいた「アウラ」「知誠館」の名を2017年より 学びの森 に変更いたしました。森には様々な命が息づいています。ここでは、学校のように誰もが同じことをめざすわけではありません。一人ひとりの子どもたちがそれぞれのペースで学び、ワクワクしながら探究を続け、自分の人生を切り拓くための答えを創り出していく。そんな豊かな学びの世界をめざします。私たちは、学びの森のような場所が社会の中に増えていくことを願っています。" style="max-width:592px;width:96%;margin:0 auto;"></p>

<?php get_footer(); ?>
