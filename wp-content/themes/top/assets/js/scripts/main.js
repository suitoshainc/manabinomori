window.$ = jQuery;
import Responsive from './Responsive';
import Wave from './Wave';
import BlockLink from './BlockLink';
import bowser from 'bowser';
/* ===============================================
Check responsive state
=============================================== */
const r = new Responsive();
//console.log(r.state()());
if(!r.isPhone()){
	$('.tel_link , .tel_link a').on('click',function(){
		return false;
	});
}
$('.nolink').on('click',function(){
	return false;
});

if(bowser.msie === true) {
	$('body').addClass('ie');
}else if(bowser.msedge === true){
	$('body').addClass('edge');
}
/* ===============================================
wave
=============================================== */
let wave1 = new Wave('wave1',35,GROBAL_WAVE_BACKGROUND,'above',2,false,90,false,9);
let wave2 = new Wave('wave2',35,'#f8f7f1','bottom',2,false,200,false,9);
wave1.init();
wave2.init();
$(window).on('resize',function(){
	wave1.setCanvas();
	wave1.draw();
	wave2.setCanvas();
	wave2.draw();
});
/* ===============================================
roolover
=============================================== */
$('img.rollover').mouseover(function(){
	$(this).attr('src',$(this).attr('src').replace(/^(.+)_off(\.[a-z]+)$/, '$1_on$2'));
}).mouseout(function(){
	$(this).attr('src',$(this).attr('src').replace(/^(.+)_on(\.[a-z]+)$/, '$1_off$2'));
}).each(function(){
	$("<img>").attr("src",$(this).attr("src").replace(/^(.+)_off(\.[a-z]+)$/, "$1_on$2"));
});
/* ===============================================
box link
=============================================== */
let borer_area_link = new BlockLink();
borer_area_link.exec('.border-area');
/* ===============================================
blank
=============================================== */
if(!$('body').hasClass('home')){
	$('a[href*="http"]').not('[href*="'+location.hostname+'"]').not('.no_icon').not('.not_blank').attr({target:"_blank"}).addClass('blank');
}
/* ===============================================
form
=============================================== */
$(document).ready(function(){
	_.each($('.basic_form').find('select'),function(item){
		let $select = $(item);

		let $dummy = $select.next('.dummy_select');

		let default_val = $select.find("option:selected").text();
		$dummy.val(default_val);

		$select.change(function(){
			let val = $(this).find("option:selected").text();
			$dummy.val(val);
		});
	});
});
