<?php get_header(); ?>
<div class="backgrond-color--lightblue nosep mt50">
	<div class="backgrond-color__inner">
		<div class="pb50 mt50">
			<p class="ac fs20 bold">お探しのページは存在しません。</p>
			<p class="ac fs18 bold mt15 pb50"><a href="/"><i class="fa fa-angle-right" aria-hidden="true"></i> トップページへ</a></p>
		</div>
	</div>
</div>
<?php get_footer(); ?>
