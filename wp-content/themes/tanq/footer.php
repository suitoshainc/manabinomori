<footer id="footer">
	<div class="footer_img">
		<div class="btns">
			<div class="inner"><a class="btn--gotop" href="#top"></a><a class="btn--inquiry" href="<?php echo home_url('contact/');?>"></a></div>
		</div>
		<div class="top">
			<canvas id="wave1"></canvas>
		</div>
		<div class="bottom">
			<canvas id="wave2"></canvas>
		</div>
	</div>
	<div class="footer_info">
		<div class="inner">
			<div class="col3 col"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6537.192930398516!2d135.5858361691815!3d34.99177128026436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x600055380af91455%3A0xb74b4398a503d0f3!2z44CSNjIxLTA4NDYg5Lqs6YO95bqc5LqA5bKh5biC5Y2X44Gk44Gk44GY44Kx5LiY5aSn6JGJ5Y-w77yS5LiB55uu77yU77yU4oiS77yZ!5e0!3m2!1sja!2sjp!4v1486801089577" width="393" height="238" frameborder="0" style="border:0" allowfullscreen></iframe></div>
			<div class="col2 col">
				<nav class="footer_nav">
					<?php include( locate_template('modules/gnavi.php', false, false ) );?>
				</nav>
				<div class="meta">
					<p>〒621-0846<br>京都府亀岡市南つつじヶ丘大葉台2-44-9<br>TEL: 0771-29-5800</p>
				</div>
				<div class="sns"><a class="no_icon" href="https://www.facebook.com/tanq.manabinomori.co.jp/" target="_blank"><span class="icon--fb no_icon">&nbsp;</span><span class="txt">facebook</span></a></div>
			</div>
			<div class="col1 col">
			<h2 class="logo"><a href="<?php echo home_url();?>">学びの森 探究スクール</a></h2>
			</div>
		</div>
	</div>
</footer>
<div id="responsive_flg"></div>
</div>

<?php if(is_page('contact')):?>
	<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
	<script>
	(function($) {
	$(function(){
		$('#zip').on('keyup',function(){
			AjaxZip3.zip2addr( 'zip', '', 'addr', 'addr');
		});
	});
	})(jQuery);

	</script>
<?php endif;?>
<?php wp_footer(); ?>
</html>
