<?php
/*
Template Name: 入会案内
*/
?>
<?php get_header(); ?>
<header class="lower_header lower_header--outline">
	<h1 class="ttl"><span>入会案内</span></h1>
</header>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post();
	switch (get_field('ol_color')) {
		case '緑':
			$area_class ='border-area--green';
			$ttl_img1 = 'border--green--head.png';
			$ttl_img2 = 'border--green--head--sp.png';
			$areatype = 'green';
			break;
		case '青':
			$area_class ='border-area--blue';
			$ttl_img1 = 'border--blue--head.png';
			$ttl_img2 = 'border--blue--head--sp.png';
			$areatype = 'blue';
			break;
		default:
			$area_class ='border-area--yellow';
			$ttl_img1 = 'border--yellow--head.png';
			$ttl_img2 = 'border--yellow--head--sp.png';
			$areatype = 'yellow';
			break;
	}
	?>
		<div class="backgrond-color--lightblue">
			<div class="backgrond-color__inner">
				<div class="content_area">
					<?php echo get_field('ol_lead');?>
				</div>
			</div>
		</div>

		<div class="backgrond-color--lightgreen backgrond-color--lightgreen--lower">
			<div class="backgrond-color__inner">
				<?php
				if( have_rows('ol_each_info') ):
					while ( have_rows('ol_each_info') ) : the_row();

					switch (get_sub_field('ol_info_size')){
						case '小':
							$modf = 'block--S';
							break;
						case '大':
							$modf = 'block--L';
							break;
						default:
							$modf = 'block--M';
							break;
					}
				?>
				<section id="<?php the_sub_field('ol_info_id');?>" class="border-area <?php echo $area_class;?> section1 border-area--lower">
					<div class="border_header">
						<img class="pc" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/<?php echo $ttl_img1;?>" alt="">
						<img class="sp" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/<?php echo $ttl_img2;?>" alt="">
						<div class="ttl_area_wrapper sp">
							<div class="ttl_area">
								<div class="ttl_prefix">
									<div class="inner">
										<p><?php the_sub_field('ol_info_grade');?></p>
									</div>
								</div>
							</div>
						</div>
						<!--ttl_area_wrapper-->
						<div class="border-body border-body--lower">
							<div class="border-body_inner" >
								<div class="sep-sp sp"></div>
								<div class="ttl_area_wrapper">
									<div class="ttl_area">
										<div class="ttl_prefix pc">
											<div class="inner">
												<p><?php the_sub_field('ol_info_grade');?></p>
											</div>
										</div>
										<div class="ttl_text">
											<h3 class="ttl"><?php the_sub_field('ol_info_lead');?></h3>
											<?php if(get_sub_field('ol_info_lead2')):?>
											<div class="lead">
												<p><?php echo get_sub_field('ol_info_lead2');?></p>
											</div>
										<?php endif;?>
									</div>
								</div>
							</div>
							<div class="content_area_wrapper">
								<div class="row">
									<?php
									if( have_rows('ol_info_details') ):
										while ( have_rows('ol_info_details') ) : the_row();
									?>
									<div class="block <?php echo $modf;?>">
										<div class="block__inner-1st">
											<div class="block__base_height">
												<div></div>
											</div>
											<div class="block__inner-2nd">
												<div class="above"><?php the_sub_field('ol_details_title');?></div>
												<?php if(get_sub_field('ol_details_icon')):?>
													<div class="icon"><?php echo get_sub_field('ol_details_icon');?></div>
												<?php endif;?>

												<?php if(have_rows('ol_details_contents')):?>
													<div class="sep">&nbsp;</div>
												<?php endif;?>

												<div class="below">
													<?php
													if( have_rows('ol_details_contents') ):
														while ( have_rows('ol_details_contents') ) : the_row();
													if(get_sub_field('ol_contents_1')){
														echo '<p class="level1">'.get_sub_field('ol_contents_1').'</p>';
													}
													if(get_sub_field('ol_contents_2')){
														echo '<p class="level2">'.get_sub_field('ol_contents_2').'</p>';
													}
													if(get_sub_field('ol_contents_3')){
														echo '<p class="level3">'.get_sub_field('ol_contents_3').'</p>';
													}
													?>
													<?php endwhile;endif;?>
												</div>
											</div>
										</div>
									</div>
									<?php endwhile;endif;?>

									<?php if( have_rows('ol_info_sub') ):?>
										<div class="below_added">
											<?php
											if( have_rows('ol_info_sub') ):
												while ( have_rows('ol_info_sub') ) : the_row();
											if(get_sub_field('ol_info_sub1')){
												echo '<p class="level1">'.get_sub_field('ol_info_sub1').'</p>';
											}
											if(get_sub_field('ol_info_sub2')){
												echo '<p class="level2">'.get_sub_field('ol_info_sub2').'</p>';
											}
											if(get_sub_field('ol_info_sub3')){
												echo '<p class="level3">'.get_sub_field('ol_info_sub3').'</p>';
											}
											?>
											<?php endwhile;endif;?>
										</div>
									<?php endif;?>
								</div>

							</div>
						</div></div>
					</div>
					<?php
					$ol_info_images = get_sub_field('ol_info_images');
					if($ol_info_images){
						$ol_info_images = wp_get_attachment_image_src( $ol_info_images, 'full');
						$width = (int)$ol_info_images[1] / 2;
						echo '<div class="inview inview--to_bottom">';
						echo '<img src="'.$ol_info_images[0].'" alt="" width="'.$width.'">';
						echo '</div>';
					}

					$ol_f_1 = get_sub_field('ol_f_1');
					if($ol_f_1){
						$ol_f_1 = wp_get_attachment_image_src( $ol_f_1, 'full');
						$width = (int)$ol_f_1[1] / 2;
						echo '<div class="inview--to_top--delay1 inview--to_top inview-f-1">';
						echo '<img src="'.$ol_f_1[0].'" alt="" width="'.$width.'">';
						echo '</div>';
					}

					$ol_f_2 = get_sub_field('ol_f_2');
					if($ol_f_2){
						$ol_f_2 = wp_get_attachment_image_src( $ol_f_2, 'full');
						$width = (int)$ol_f_2[1] / 2;
						echo '<div class="inview--to_top--delay2 inview--to_top inview-f-2 pc">';
						echo '<img src="'.$ol_f_2[0].'" alt="" width="'.$width.'">';
						echo '</div>';
					}

					$ol_f_3 = get_sub_field('ol_f_3');
					if($ol_f_3){
						$ol_f_3 = wp_get_attachment_image_src( $ol_f_3, 'full');
						$width = (int)$ol_f_3[1] / 2;
						echo '<div class="inview--to_top--delay3 inview--to_top inview-f-3 pc">';
						echo '<img src="'.$ol_f_3[0].'" alt="" width="'.$width.'">';
						echo '</div>';
					}
					?>
				</section>
			<?php endwhile;endif;?>
			<?php
			$ol_t_name = get_field('ol_t_name');
			$ol_t_prof = get_field('ol_t_prof');
			$ol_t_photo = get_field('ol_t_photo');
			if($ol_t_name && $ol_t_prof && $ol_t_photo):
				$ol_t_photo = wp_get_attachment_image_src( $ol_t_photo, 'full');
				$width = (int)$ol_t_photo[1] / 2;
			?>
			<section id="guide_prof" class="guide_prof">
				<div class="inner">
					<div class="img">
						<img src="<?php echo $ol_t_photo[0];?>" alt="" width="<?php echo $width;?>">
					</div>
					<div class="txtarea">
						<div class="inner">
							<h3 class="ttl"><?php echo $ol_t_name;?></h3>
							<div class="txt">
								<p><?php echo $ol_t_prof;?></p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php endif;?>
		</div>
	</div>

	<div class="backgrond-color--lightblue backgrond-color--lightblue--lower nosep <?php echo $areatype;?>">
		<div class="backgrond-color__inner">
			<?php echo get_field('ol_info_below');?>
		</div>
	</div>
<?php endwhile; ?>
<?php else : ?>
	<h2 class="title">記事が見つかりませんでした。</h2>
	<p>検索で見つかるかもしれません。</p><br />
	<?php get_search_form(); ?>
<?php endif; ?>


<?php get_footer(); ?>
