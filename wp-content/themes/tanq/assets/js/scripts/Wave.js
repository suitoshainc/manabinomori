/* ===============================================
wave
=============================================== */
class Wave{
	constructor(target = '#wave' , height = 300 ,background = '#000',vertical = 'above',zoom = 2,reverse = false,speed = 140,repeat = true , waveHeight = 3) {
		this.target = target;
		this.height = height;
		this.background = background;
		this.vertical = vertical;
		this.zoom = zoom;
		this.reverse = reverse;
		this.speed = speed;
		this.unit = 100;
		this.drawSeconds = 0;
		this.drawT = 0;
		this.repeat = repeat;
		this.waveHeight = waveHeight;
	}

	setCanvas(){
		this.base_height = this.height;
		this.base_width = document.documentElement.clientWidth;

		this.canvas = document.getElementById(this.target);

		if(!this.canvas){
			return false;
		};

		this.canvas.width = this.base_width; //Canvasのwidthをウィンドウの幅に合わせる
		this.canvas.height = this.base_height;//高さ

		this.context = this.canvas.getContext("2d");

		if(this.reverse === true){//反転
			this.context.transform(-1, 0, 0, 1, this.base_width, 0);
		}

		this.height = this.canvas.height;
		this.width = this.canvas.width;

		this.xAxis = Math.floor(this.height/2);
		this.yAxis = 0;

	}

	init() {
		this.setCanvas();
		this.draw();
	}

	draw() {
		// キャンバスの描画をクリア
		this.context.clearRect(0, 0, this.base_width, this.base_height);
		//波を描画
		this.drawWave(this.background, 1, this.zoom, 0);
		// Update the time and draw again
		this.drawSeconds = this.drawSeconds + .009;
		this.drawT = this.drawSeconds * Math.PI;
		if(this.repeat){
			this.drawRepeat();
		}
	};

	drawRepeat(){
		setTimeout(() => {
			this.draw();
		}, this.speed);
	}

	/**
	* 波を描画
	* drawWave(色, 不透明度, 波の幅のzoom, 波の開始位置の遅れ)
	*/
	drawWave(color, alpha, zoom, delay) {
		this.context.fillStyle = color;
		this.context.globalAlpha = alpha;

		this.context.beginPath(); //パスの開始
		this.drawSine(this.drawT / 0.5, zoom, delay);

		if(this.vertical == 'above'){//波を下向きに
			this.context.lineTo(this.base_width + 10, 0);
			this.context.lineTo(0, 0);
		}else if(this.vertical == 'bottom'){//波を上向きに
			this.context.lineTo(this.base_width + 10, this.base_height);
			this.context.lineTo(0, this.base_height);
		}

		this.context.closePath() //パスを閉じる
		this.context.fill(); //塗りつぶす
	}
	/**
	 * Function to draw sine
	 *
	 * The sine curve is drawn in 10px segments starting at the origin.
	 * drawSine(時間, 波の幅のzoom, 波の開始位置の遅れ)
	 */
	drawSine(t, zoom, delay) {
		// Set the initial x and y, starting at 0,0 and translating to the origin on
		// the canvas.
		let i = this.yAxis;
		var x = t; //時間を横の位置とする
		var y = Math.sin(x) / zoom;
		this.context.moveTo(this.yAxis, this.unit * y + this.xAxis); //スタート位置にパスを置く
		// Loop to draw segments (横幅の分、波を描画)
		for (i = this.yAxis; i <= this.width + 10; i += 10) {
			x = t+(-this.yAxis+i)/this.unit/zoom;
			y = Math.sin(x - delay) / this.waveHeight;
			this.context.lineTo(i, this.unit * y + this.xAxis);
		}
	}
}
module.exports = Wave;
