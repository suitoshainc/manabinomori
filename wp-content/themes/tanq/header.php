<?php
$url = parse_url( $_SERVER['REQUEST_URI'] );
if($url['path'] === '/blog/category/cat5/'){
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: http://free.manabinomori.co.jp/blog/category/cat5");
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="shortcut icon" href="<?php echo home_url('/favicon.ico')?>" />
	<?php if(wpmd_is_tablet()):?>
	<meta id="viewport" content="width=1400" name="viewport">
	<?php else:?>
	<meta id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0,user-scalable=1" name="viewport">
	<?php endif;?>
	<?php wp_head(); ?>

	<?php
	if(!is_home() || is_404()):?>
		<script>
			var GROBAL_WAVE_BACKGROUND = '#EEF6F8';
		</script>
	<?php else: ?>
		<script>
			var GROBAL_WAVE_BACKGROUND = '#f5fbef';
		</script>
	<?php endif;?>
</head>
<body id="top" <?php echo body_class();?>>
	<div id="wrap">
		<nav class="spnavi sp" id="spnavi">
			<div id="scroller">
				<?php include( locate_template('modules/gnavi.php', false, false ) );?>
			</div>
		</nav>
		<div class="btn_sp_navi_wrap"><a class="btn_sp_navi nosc nosms" href="#sp"><span class="bar"></span><span class="bar"></span><span class="bar"></span><span class="text">MENU</span></a></div>
		<header class="main_header">
			<a href="<?php echo home_url();?>">
			<h1 class="logo" title="学びの森 探究スクール">
				<span>学びの森 探究スクール</span>
			</h1>
			</a>
			<aside class="tel"><a class="tel_link" href="tel:0771-29-5800">TEL : 0771-29-5800</a></aside>
			<div class="leaf1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_header--leaf_1.png" alt=""></div>
			<div class="leaf2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/img_header--leaf_2.png" alt=""></div>
		</header>
		<nav class="gnav">
			<?php include( locate_template('modules/gnavi.php', false, false ) );?>
		</nav>
