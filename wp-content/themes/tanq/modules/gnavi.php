<ul>
	<li><a href="<?php echo home_url('/concept/');?>">コンセプト</a></li>
	<li><a href="" class="nolink" style="color:#565a55!important;">入会案内</a>
		<ul class="child">
			<li><a href="<?php echo home_url('/manabi1/');?>">小学生</a></li>
			<li><a href="<?php echo home_url('/manabi2/');?>">中学生</a></li>
			<li><a href="<?php echo home_url('/manabi3/');?>">高校生</a></li>
		</ul>
	</li>
	<li><a href="<?php echo home_url('/sotsugyo/');?>">卒業生より</a></li>
	<li><a href="<?php echo home_url('/teacher/');?>">先生たち</a></li>
	<li><a href="<?php echo home_url('/access/');?>">アクセスと概要</a></li>
	<li><a href="<?php echo home_url('/contact/');?>">お問い合わせ</a></li>
	<li class="fb"><a href="https://www.facebook.com/tanq.manabinomori.co.jp/" target="_blank">Facebook</a></li>
</ul>
